package org.bitbucket.afx.utils;

import static org.junit.Assert.*;

import org.bitbucket.afx.tests.JavaFxJUnit4ClassRunner;
import org.bitbucket.afx.utils.AFXUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

@RunWith(JavaFxJUnit4ClassRunner.class)
public class AFXUtilsTest {
	
	@Test
	public void testFindField() {
		Node rootNode = AFXUtils.loadFxml("/fxml/MainWithController.fxml");
		
		String id = "user.username";
		Node node = AFXUtils.findNodeById(rootNode, id);
		
		assertNotNull(node);
		assertTrue(id.equals(node.getId()));
	}
	
	@Test
	public void testLoadFXML() {
		Node root = AFXUtils.loadFxml("/fxml/MainWithController.fxml");
		assertNotNull(root);
		assertTrue(root instanceof BorderPane);
	}

	@Test
	public void testSetLabelText() {
		Node root = AFXUtils.loadFxml("/fxml/MainWithController.fxml");
		assertNotNull(root);
		
		Node node = AFXUtils.findNodeById(root, "label");
		assertNotNull(node);
		assertTrue(node instanceof Label);
		Label label = (Label) node;
		assertEquals("text", label.textProperty().get());
		
		// change label text
		AFXUtils.setLabelText("label", "newText", root);
		assertEquals("newText", label.textProperty().get());
	}
}
