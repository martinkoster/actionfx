package org.bitbucket.afx.spring;

import org.bitbucket.afx.annotation.FxController;
import org.bitbucket.afx.annotation.FxView;

/**
 * Test controller carrying <tt>@FxController</tt> and <tt>@FxView</tt> annotations
 * for unit-testing.
 * 
 * @author MartinKoster
 *
 */
@FxController("testController")
@FxView(id="testView", fxml="/fxml/MainWithoutController.fxml")
public class TestController {

}
