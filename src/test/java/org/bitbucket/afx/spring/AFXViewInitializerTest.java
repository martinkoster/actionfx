package org.bitbucket.afx.spring;

import static org.junit.Assert.*;
import javafx.scene.layout.BorderPane;

import org.bitbucket.afx.tests.JavaFxSpringJUnit4ClassRunner;
import org.bitbucket.afx.view.AbstractView;
import org.bitbucket.afx.view.fxml.FxmlWindowView;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@RunWith(JavaFxSpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/applicationContext-test.xml")
public class AFXViewInitializerTest {

	private AbstractView view;
	
	@Autowired
	private AFXViewInitializer afxViewInitializer;
	
	 @Before
	public void onSetup() {
		this.view = this.createWindowView();
	}
	
	@Test
	public void testPostProcessView() {
		this.afxViewInitializer.postProcessView(view, null);
		
		// check whether FXML is loaded and the important properties are set
		assertNotNull(view.getNode());
		assertTrue(view.getNode() instanceof BorderPane);
		assertNotNull(view.getMessageSource());
		assertNotNull(view.getPrimaryStageHolder());
		assertNotNull(view.getComponentCache());
	}

	/**
	 * Construct the class under test, use this test case as model holder,
	 * and initialize the bindings.
	 * 
	 * @return
	 */
	private FxmlWindowView createWindowView() {
		FxmlWindowView view = new FxmlWindowView();
		view.setId("windowView");
		view.setFxml("/fxml/MainWithController.fxml");
		return view;
	}

	public AFXViewInitializer getAfxViewInitializer() {
		return afxViewInitializer;
	}

	public void setAfxViewInitializer(AFXViewInitializer afxViewInitializer) {
		this.afxViewInitializer = afxViewInitializer;
	}

	
}
