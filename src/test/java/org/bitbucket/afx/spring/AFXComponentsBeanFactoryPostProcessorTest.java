package org.bitbucket.afx.spring;

import org.bitbucket.afx.tests.JavaFxSpringJUnit4ClassRunner;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;


@RunWith(JavaFxSpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/applicationContext-test.xml")
public class AFXComponentsBeanFactoryPostProcessorTest implements ApplicationContextAware {

	@Autowired
	private AFXComponentsBeanFactoryPostProcessor factoryPostProcessor;
	
	private ApplicationContext applicationContext;
	
	/**
	 *  Check for availability of required ActionFX Spring beans
	 */
	@Test
	public void testJFXComponentsBeanFactoryPostProcessor() {
		assertNotNull(applicationContext);
		assertNotNull(factoryPostProcessor);
		assertNotNull(factoryPostProcessor.getMessageSource());
		assertNotNull(applicationContext.getBean(AFXComponentsBeanFactoryPostProcessor.BEANNAME_AFXCONTROLLERPOSTPROCESSOR));
		assertNotNull(applicationContext.getBean(AFXComponentsBeanFactoryPostProcessor.BEANNAME_AFXVIEWINITIALIZER));
	}

	public AFXComponentsBeanFactoryPostProcessor getFactoryPostProcessor() {
		return factoryPostProcessor;
	}

	public void setFactoryPostProcessor(
			AFXComponentsBeanFactoryPostProcessor factoryPostProcessor) {
		this.factoryPostProcessor = factoryPostProcessor;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {

		this.applicationContext = applicationContext;
	}

}
