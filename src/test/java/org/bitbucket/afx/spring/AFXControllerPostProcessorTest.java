package org.bitbucket.afx.spring;

import static org.junit.Assert.*;
import javafx.scene.layout.BorderPane;

import org.bitbucket.afx.tests.JavaFxSpringJUnit4ClassRunner;
import org.bitbucket.afx.view.fxml.FxmlWindowView;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.test.context.ContextConfiguration;

@RunWith(JavaFxSpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/spring/applicationContext-test.xml")
public class AFXControllerPostProcessorTest implements ApplicationContextAware {

	@Autowired
	private AFXControllerPostProcessor postProcessor;

	private ApplicationContext applicationContext;

	private FxmlWindowView view;

	@Before
	public void onSetup() {
		// add a view instance to the Spring context. This is
		// usually done by AFXComponentsBeanFactoryPostProcessor.
		// We need to mock this instance.
		this.view = new FxmlWindowView();
		view.setId("testView");
		view.setFxml("/fxml/MainWithoutController.fxml");
		((ConfigurableListableBeanFactory) ((GenericApplicationContext) applicationContext)
				.getBeanFactory()).registerSingleton(view.getId(), view);
	}

	@Test
	public void testPostProcessAfterInitialization() {
		this.postProcessor.postProcessAfterInitialization(new TestController(),
				null);

		// validate that the view has been enriched by the postProcessor
		assertNotNull(view.getNode());
		assertTrue(view.getNode() instanceof BorderPane);
		assertNotNull(view.getMessageSource());
		assertNotNull(view.getPrimaryStageHolder());
		assertNotNull(view.getComponentCache());
	}

	public AFXControllerPostProcessor getPostProcessor() {
		return postProcessor;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

}
