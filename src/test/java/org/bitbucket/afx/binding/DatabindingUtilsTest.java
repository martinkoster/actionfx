package org.bitbucket.afx.binding;

import static org.junit.Assert.*;

import java.util.Map;

import org.bitbucket.afx.tests.JavaFxJUnit4ClassRunner;
import org.bitbucket.afx.utils.AFXUtils;
import org.bitbucket.afx.view.binding.DatabindingUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

import javafx.scene.Node;

@RunWith(JavaFxJUnit4ClassRunner.class)
public class DatabindingUtilsTest {
	
	@Test
	public void testFieldExtraction() {
		Node rootNode = AFXUtils.loadFxml("/fxml/MainWithController.fxml");
		Map<String, Object> values = DatabindingUtils.extractValues(rootNode);
		assertNotNull(values);
		assertTrue(values.size() >= 5);
	}


	
}
