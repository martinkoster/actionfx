package org.bitbucket.afx.view.fxml;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import org.bitbucket.afx.annotation.FxBinding;
import org.bitbucket.afx.samples.User;
import org.bitbucket.afx.tests.JavaFxJUnit4ClassRunner;
import org.bitbucket.afx.utils.AFXUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Test class for <tt>FxmlWindowView</tt>.
 * 
 * @author MartinKoster
 *
 */
@RunWith(JavaFxJUnit4ClassRunner.class)
public class FxmlWindowViewTest {

	// the view under test
	private FxmlWindowView view;
	
	// "user" is referenced via a nested path from the FXML, e.g. "user.password".
	private User user = new User();

	// "checkBox" is the name of a CheckBox in Main.FXML
	private Boolean checkBox = Boolean.TRUE;

	// "tableItems" is the name of a TableView in Main.FXML
	private List<String> tableItems = new ArrayList<String>();

	// perform binding from model to FXML, in this case, get the selected item from a TableView
	@FxBinding(nodeId="tableItems", propertyMethod="selectionModel.selectedItem")
	private String selectedTableViewItem;
	
	@Before
	public void onSetup() {
		
		// create table view items to be added to the TableView
		this.tableItems.add("item1");
		this.tableItems.add("item2");
		this.tableItems.add("item3");
		
		// construct the class under test
		this.view = this.createWindowView();
	}

	/**
	 * Test databinding with a <tt>javafx.scene.control.TextField</tt>.
	 */
	@Test
	public void testDatabindingTextField() {
		
		TextField t = (TextField) AFXUtils.findNodeById(view.getNode(), "user.username");
		assertNotNull(t);
		t.textProperty().set("test-username");
		
		// bound value transferred?
		assertEquals(user.getUsername(), "test-username");
	}

	/**
	 * Test databinding with a <tt>javafx.scene.control.PasswordField</tt>.
	 */
	@Test
	public void testDatabindingPasswordField() {
		PasswordField p = (PasswordField) AFXUtils.findNodeById(view.getNode(), "user.password");
		assertNotNull(p);
		p.textProperty().set("test-password");
		
		// bound value transferred?
		assertEquals(user.getPassword(), "test-password");
	}
	
	/**
	 * Test databinding with a <tt>javafx.scene.control.CheckBox</tt>.
	 */
	@Test
	public void testDatabindingCheckbox() {
		FxmlWindowView view = this.createWindowView();
		
		CheckBox b = (CheckBox) AFXUtils.findNodeById(view.getNode(), "checkBox");
		assertNotNull(b);

		// bound value transferred?
		b.selectedProperty().set(false);
		assertEquals(checkBox.booleanValue(), false);

		// bound value transferred?
		b.selectedProperty().set(true);
		assertEquals(checkBox.booleanValue(), true);
	}
	
	/**
	 * Test databinding with a <tt>javafx.scene.control.TableView</tt>.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void testDatabindingTableView() {
		TableView<String> v = (TableView<String>) AFXUtils.findNodeById(view.getNode(), "tableItems");
		
		// validate that the items have been bound from this modelHolder
		assertNotNull(v.getItems());
		assertEquals(v.getItems().size(), 3);
		assertEquals(v.getItems().get(0), "item1");
		assertEquals(v.getItems().get(1), "item2");
		assertEquals(v.getItems().get(2), "item3");
		
		// assert that selectedTableViewItem is still null.
		assertNull(this.selectedTableViewItem);
		
		// select item with index "1" ...
		v.getSelectionModel().clearAndSelect(1);
		
		// ...and check our attribute annotated by @FxBinding
		assertNotNull(this.selectedTableViewItem);
		assertEquals(this.selectedTableViewItem, "item2");
		
		// do the same with index "0"
		v.getSelectionModel().clearAndSelect(0);
		assertNotNull(this.selectedTableViewItem);
		assertEquals(this.selectedTableViewItem, "item1");

		// ... and index "2"
		v.getSelectionModel().clearAndSelect(2);
		assertNotNull(this.selectedTableViewItem);
		assertEquals(this.selectedTableViewItem, "item3");
		
		// clear selection
		v.getSelectionModel().clearSelection();
		assertNull(this.selectedTableViewItem);
	}
	
	/**
	 * Construct the class under test, use this test case as model holder,
	 * and initialize the bindings.
	 * 
	 * @return
	 */
	private FxmlWindowView createWindowView() {
		FxmlWindowView view = new FxmlWindowView();
		view.setNode(AFXUtils.loadFxml("/fxml/MainWithController.fxml"));
		view.setId("windowView");
		view.setModel(this);
		view.initBindings();
		return view;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getCheckBox() {
		return checkBox;
	}

	public void setCheckBox(Boolean checkBox) {
		this.checkBox = checkBox;
	}

	public List<String> getTableItems() {
		return tableItems;
	}

	public void setTableItems(List<String> tableItems) {
		this.tableItems = tableItems;
	}

	public String getSelectedTableViewItem() {
		return selectedTableViewItem;
	}

	public void setSelectedTableViewItem(String selectedTableViewItem) {
		this.selectedTableViewItem = selectedTableViewItem;
	}
	
}
