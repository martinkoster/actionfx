package org.bitbucket.afx.view;

import static org.junit.Assert.*;

import java.util.List;

import javafx.scene.Node;

import org.bitbucket.afx.tests.JavaFxJUnit4ClassRunner;
import org.bitbucket.afx.utils.AFXUtils;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JavaFxJUnit4ClassRunner.class)
public class NodeWrapperTest {

	@Test
	public void testTextfieldPrimaryValue() {
		Node rootNode = AFXUtils.loadFxml("/fxml/MainWithController.fxml");
		Node node = AFXUtils.findNodeById(rootNode, "user.username");
		NodeWrapper wrapper = new NodeWrapper(node);
		Object value = wrapper.getPrimaryValue();
		assertTrue(value != null && "".equals(value));
	}
	
	@Test
	public void testGetChildren() {
		Node rootNode = AFXUtils.loadFxml("/fxml/MainWithController.fxml");
		Node node = AFXUtils.findNodeById(rootNode, "MainWindowBorderPane");
		assertNotNull(node);
		NodeWrapper wrapper = new NodeWrapper(node);
		List<Node> children = wrapper.getChildren();
		assertNotNull(children);
		assertTrue(children.size() >= 4);
	}

}
