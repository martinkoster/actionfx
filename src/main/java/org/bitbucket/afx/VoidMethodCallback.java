package org.bitbucket.afx;

/**
 * Callback interface for using simple lambda expressions in a void-method-context.
 * 
 * @author MartinKoster
 *
 */
@FunctionalInterface
public interface VoidMethodCallback {

	/**
	 * The void-method to execute.
	 * 
	 * @throws Exception
	 */
	public void executeMethod() throws Exception;
}
