package org.bitbucket.afx.view;

import org.bitbucket.afx.view.behavior.ViewStateListener;

import javafx.scene.Node;

/**
 * Interface for any type of JavaFX view. It provides access to the root node of
 * a JavaFX object tree.
 * 
 * @author MartinKoster
 *
 */
public interface View {

	/**
	 * Gets the view ID.
	 * 
	 * @return the view ID
	 */
	public String getId();
	
	/**
	 * Gets the root node of the JavaFX scene graph.
	 * 
	 * @return the root node of the JavaFX scene graph
	 */
	public Node getNode();
	
	/**
	 * Shows the view.
	 */
	public void show();
	
	/**
	 * Adds a {@link ViewStateListener} to this view.
	 * @param listener the lister to be attached to this view
	 */
	public void addListener(ViewStateListener listener);
}
