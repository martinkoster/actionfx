package org.bitbucket.afx.view;

import org.controlsfx.dialog.ExceptionDialog;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;

/**
 * Utility class for handling dialogues.
 * 
 * @author Martin
 *
 */
public class DialogUtils {

	/**
	 * Displays a modal error dialogue with the specified <tt>title</tt> and
	 * <tt>message</tt>.
	 * 
	 * @param title the title of the dialog
	 * @param message the message to be displayed in the dialog
	 */
	public static void showErrorDialog(String title, String message) {
		Platform.runLater(() -> {
			Alert dialog = new Alert(AlertType.ERROR);
			dialog.setTitle(title);
			dialog.setContentText(message);
			dialog.initModality(Modality.APPLICATION_MODAL);
			// alert.initOwner(window);
			dialog.showAndWait();
		});
	}

	/**
	 * Displays a modal error dialogue including a given <tt>exception</tt>.
	 * 
	 * @param title the title of the dialog
	 * @param message the message to be displayed in the dialog
	 * @param exception the exception to be displayed in the dialog
	 */
	public static void showExceptionDialog(String title, String message, Throwable exception) {
		Platform.runLater(() -> {
			ExceptionDialog dialog = new ExceptionDialog(exception);
			dialog.setTitle(title);
			dialog.setContentText(message);
			dialog.initModality(Modality.APPLICATION_MODAL);
			dialog.showAndWait();
		});
	}
}
