package org.bitbucket.afx.view.behavior.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.view.NodeHandler;
import org.bitbucket.afx.view.NodeWrapper;

import javafx.scene.Node;

public class ViewHandlers {

	private static final Log LOG = LogFactory.getLog(ViewHandlers.class);

	/**
	 * <tt>NodeHandler</tt> implementation that enables a given node.
	 * 
	 * @author Martin
	 *
	 */
	public static final class EnableHandler implements NodeHandler {
		@Override
		public void process(Node node) {
			LOG.debug("Enabling node with id='" + node.getId() + "'");
			node.setDisable(false);
		}
	}

	/**
	 * <tt>NodeHandler</tt> implementation that disables a given node.
	 * 
	 * @author Martin
	 *
	 */
	public static final class DisableHandler implements NodeHandler {
		@Override
		public void process(Node node) {
			LOG.debug("Disabling node with id='" + node.getId() + "'");
			node.setDisable(true);
		}
	}

	/**
	 * <tt>NodeHandler</tt> implementation that hides a given node.
	 * 
	 * @author Martin
	 *
	 */
	public static final class HideHandler implements NodeHandler {
		@Override
		public void process(Node node) {
			LOG.debug("Hiding node with id='" + node.getId() + "'");
			node.setVisible(false);
		}
	}

	/**
	 * <tt>NodeHandler</tt> implementation that shows a given node.
	 * 
	 * @author Martin
	 *
	 */
	public static final class ShowHandler implements NodeHandler {
		@Override
		public void process(Node node) {
			LOG.debug("Showing node with id='" + node.getId() + "'");
			node.setVisible(true);
		}
	}

}