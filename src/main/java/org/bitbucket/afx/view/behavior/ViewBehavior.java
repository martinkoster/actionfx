package org.bitbucket.afx.view.behavior;

import javafx.scene.Node;

/**
 * Interface for specifying a view behavior.
 * 
 * @author MartinKoster
 *
 */
public interface ViewBehavior {

	/**
	 * Evaluate this <tt>ViewBehavior</tt> on the given scene graph represented by
	 * the <tt>root</tt> node and apply the defined behavior on it.
	 * 
	 * @param root the root node representing the scene graph of JavaFX
	 */
	public void evaluateAndApply(Node root);
}
