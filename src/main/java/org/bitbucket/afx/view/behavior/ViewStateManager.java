package org.bitbucket.afx.view.behavior;

import javafx.beans.value.ChangeListener;

/**
 * Interface for controlling the behavior and view state of elements within the view.
 * 
 * @author Martin
 *
 */
public interface ViewStateManager {

	/**
	 * Enables the node under the given <tt>nodeId</tt>.
	 * 
	 * @param nodeId the node ID 
	 */
	public ViewStateManager enable(String nodeId);
	
	/**
	 * Enables the node under the given <tt>nodeId</tt>.
	 * 
	 * @param nodeId the node ID 
	 */
	public ViewStateManager disable(String nodeId);
	
	/**
	 * Hides the node under the given <tt>nodeId</tt>.
	 * 
	 * @param nodeId the node ID 
	 */
	public ViewStateManager hide(String nodeId);
	
	/**
	 * Shows the node under the given <tt>nodeId</tt>.
	 * 
	 * @param nodeId the node ID 
	 */
	public ViewStateManager show(String nodeId);
	
	/**
	 * Installs a <tt>ChangeListener</tt> for the node under <tt>nodeId</tt>.
	 * 
	 * @param nodeId the node ID 
	 * @param listener the listener to be installed to the given <tt>nodeId</tt>
	 */
	public <T> void install(String nodeId, ChangeListener<T> listener);
		
	/**
	 * Installs a <tt>ChangeListener</tt> for the node under <tt>nodeId</tt> for the property specified
	 * with <tt>propertyMethodName</tt>.
	 * 
	 * @param nodeId the node ID 
	 * @param propertyMethodName the name of the propery method
	 * @param listener the listener to be installed to the given <tt>nodeId</tt> and <tt>propertyMethodName</tt>
	 */
	public <T> void install(String nodeId, String propertyMethodName, ChangeListener<T> listener);
	
	/**
	 * Adds a defined <tt>ViewBehavior</tt> to this <tt>ViewStateManager</tt>.
	 * 
	 * @param viewBehavior the view behavior to be added to this <tt>ViewStateManager</tt>
	 */
	public void addViewBehavior(ViewBehavior viewBehavior);
}
