package org.bitbucket.afx.view.behavior;

import javafx.collections.ListChangeListener.Change;
import javafx.scene.Node;

/**
 * Container class providing details about the type of change that occured within the view. 
 * <p>
 * Used by <tt>ViewStateListener</tt> to generalize change events.
 * 
 * @author Martin
 *
 */
public class ChangeDetails {

	/**
	 * Enum describing the type of change. Supported are
	 * field and list changes as of the moment.
	 * 
	 * @author MartinKoster
	 *
	 */
	public enum ChangeType {
		FIELD, LIST;
	}	
	
	private ChangeType changeType;
	
	private Node changedNode;
	
	private Object oldValue;
	
	private Object newValue;
	
	private Change<?> listChange;

	/**
	 * Returns the <tt>ChangeType</tt>.
	 * 
	 * @return the <tt>ChangeType</tt>
	 */
	public ChangeType getChangeType() {
		return changeType;
	}

	/**
	 * Sets the <tt>ChangeType</tt>.
	 * 
	 * @param changeType the <tt>ChangeType</tt> to set
	 */
	public void setChangeType(ChangeType changeType) {
		this.changeType = changeType;
	}

	/**
	 * Returns the node that this <tt>ChangeDetails</tt> have been generated for.
	 * 
	 * @return the node that this <tt>ChangeDetails</tt> have been generated for
	 */
	public Node getChangedNode() {
		return changedNode;
	}

	/**
	 * Sets the node that this <tt>ChangeDetails</tt> have been generated for
	 * 
	 * @param changedNode the node that this <tt>ChangeDetails</tt> have been generated for
	 */
	public void setChangedNode(Node changedNode) {
		this.changedNode = changedNode;
	}

	/**
	 * Returns the old, previously set value. 
	 * 
	 * @return the old, previously set value
	 */
	public Object getOldValue() {
		return oldValue;
	}

	/**
	 * Sets the old, previously set value.
	 * 
	 * @param oldValue the old, previously set value
	 */
	public void setOldValue(Object oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * Returns the newly set value.
	 * 
	 * @return the newly set value
	 */
	public Object getNewValue() {
		return newValue;
	}

	/**
	 * Sets the newly set value.
	 * 
	 * @param newValue the newly set value
	 */
	public void setNewValue(Object newValue) {
		this.newValue = newValue;
	}

	/**
	 * Returns the <tt>javafx.collections.Change</tt> instance, in case the given <tt>ChangeDetails</tt> have been generated
	 * for a list change event.
	 * 
	 * @return the <tt>javafx.collections.Change</tt> instance
	 */
	public Change<?> getListChange() {
		return listChange;
	}

	/**
	 * Sets the <tt>Change</tt> instance, in case the given <tt>ChangeDetails</tt> have been generated
	 * for a list change event.
	 * 
	 * @param listChange the <tt>javafx.collections.Change</tt> instance
	 */
	public void setListChange(Change<?> listChange) {
		this.listChange = listChange;
	}
}
