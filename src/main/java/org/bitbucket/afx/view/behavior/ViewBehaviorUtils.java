package org.bitbucket.afx.view.behavior;

import org.bitbucket.afx.view.behavior.impl.ConditionBasedViewBehavior;
import org.bitbucket.afx.view.behavior.impl.ViewConditions;
import org.bitbucket.afx.view.behavior.impl.ViewHandlers;
import org.bitbucket.afx.view.validation.ValidationHandler;

/**
 * Utility class that provides some out-of-box behaviors.
 * 
 * @author MartinKoster
 *
 */
public class ViewBehaviorUtils {

	/**
	 * <tt>ViewBehavior</tt> that enables the fields in <tt>nodesToEnable</tt>, if the fields in <tt>nodesToCheck</tt>
	 * have values.
	 * 
	 * @param nodesToEnable a list of nodes to be enabled
	 * @param nodesToCheck a list of nodes to be checked
	 * 
	 * @return the constructed instance of <tt>ViewBehavior</tt>
	 */
	public static ViewBehavior enableIfValuesProvided(String[] nodesToEnable, String[] nodesToCheck) {
		return new ConditionBasedViewBehavior().addCondition(new ViewConditions.AllValuesProvidedCondition())
				.addNodesToCheck(nodesToCheck).addNodeHandler(new ViewHandlers.EnableHandler())
				.addNodesToHandle(nodesToEnable);
	}

	/**
	 * <tt>ViewBehavior</tt> that disables the fields in <tt>nodesToEnable</tt>, if at least one of the fields in <tt>nodesToCheck</tt>
	 * has no value.
	 * 
	 * @param nodesToDisable a list of nodes to be disabled
	 * @param nodesToCheck a list of nodes to be checked
	 * 
	 * @return the constructed instance of <tt>ViewBehavior</tt>
	 */
	public static ViewBehavior disableIfOneValueMissing(String[] nodesToDisable, String[] nodesToCheck) {
		return new ConditionBasedViewBehavior().addCondition(new ViewConditions.AtLeastOneValueMissingCondition())
				.addNodesToCheck(nodesToCheck).addNodeHandler(new ViewHandlers.DisableHandler())
				.addNodesToHandle(nodesToDisable);
	}
	
	/**
	 * <tt>ViewBehavior</tt> that enables the fields in <tt>nodesToEnable</tt>, if the fields in <tt>nodesToCheck</tt> are valid.
	 * 
	 * @param nodesToEnable a list of nodes to be enabled
	 * @param nodesToCheck a list of nodes to be checked
	 * @param validationHandler the <tt>ValidationHandler</tt> that is used to determine whether view values are valid or not
	 * 
	 * @return the constructed instance of <tt>ViewBehavior</tt>
	 */
	public static ViewBehavior enableIfValuesValid(String[] nodesToEnable, String[] nodesToCheck, ValidationHandler validationHandler) {
		return new ConditionBasedViewBehavior().addCondition(new ViewConditions.NoValidationErrorCondition(validationHandler))
				.addNodesToCheck(nodesToCheck).addNodeHandler(new ViewHandlers.EnableHandler())
				.addNodesToHandle(nodesToEnable);
	}
	
	/**
	 * <tt>ViewBehavior</tt> that disables the fields in <tt>nodesToEnable</tt>, if the fields in <tt>nodesToCheck</tt> are not valid.
	 * 
	 * @param nodesToEnable a list of nodes to be enabled
	 * @param nodesToCheck a list of nodes to be checked
	 * @param validationHandler the <tt>ValidationHandler</tt> that is used to determine whether view values are valid or not
	 * 
	 * @return the constructed instance of <tt>ViewBehavior</tt>
	 */
	public static ViewBehavior disableIfValuesInvalid(String[] nodesToEnable, String[] nodesToCheck, ValidationHandler validationHandler) {
		return new ConditionBasedViewBehavior().addCondition(new ViewConditions.ValidationErrorCondition(validationHandler))
				.addNodesToCheck(nodesToCheck).addNodeHandler(new ViewHandlers.DisableHandler())
				.addNodesToHandle(nodesToEnable);
	}

	/**
	 * <tt>ViewBehavior</tt> that disables the fields in <tt>nodesToDisable</tt>, if all values are provided in the fields in <tt>nodesToCheck</tt>
	 * has no value.
	 * 
	 * @param nodesToDisable a list of nodes to be disabled
	 * @param nodesToCheck a list of nodes to be checked
	 * 
	 * @return the constructed instance of <tt>ViewBehavior</tt>
	 */
	public static ViewBehavior disableIfAllValuesProvided(String[] nodesToDisable, String[] nodesToCheck) {
		return new ConditionBasedViewBehavior().addCondition(new ViewConditions.AllValuesProvidedCondition())
				.addNodesToCheck(nodesToCheck).addNodeHandler(new ViewHandlers.DisableHandler())
				.addNodesToHandle(nodesToDisable);
	}
	
}
