package org.bitbucket.afx.view.behavior;

import javafx.scene.Node;

/**
 * Bass class for conditions.
 * 
 * @author Martin
 *
 */
public interface ViewCondition {
	
	/**
	 * Abstract method to be implemented by subclasses in order to check, whether the
	 * nodes specified in <tt>nodesToCheck</tt> meet a specific condition. 
	 * 
	 * @param root the root node of the scene graph of JavaFX
	 * @param nodesToCheck the nodes that shall be checked by the implementing  class
	 * 
	 * @return <tt>true</tt>, if the nodes in <tt>nodesToCheck</tt> satisfy the condition, <tt>false</tt> otherwise.
	 */
	public abstract boolean meets(Node root, String[] nodesToCheck);
}