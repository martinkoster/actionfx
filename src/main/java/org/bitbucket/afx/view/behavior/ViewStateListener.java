package org.bitbucket.afx.view.behavior;

import javafx.scene.Node;

/**
 * Listener interface for reacting on changes in the view state.
 * 
 * @author Martin
 *
 */
public interface ViewStateListener {

	/**
	 * Callback method invoked when the view state is changed.
	 * 
	 * @param rootNode the root node of the scene graph of JavaFX
	 * @param changeDetails the container that provides details on the change itself
	 */
	public void onStateChange(Node rootNode, ChangeDetails changeDetails);
	
}
