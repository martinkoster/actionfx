package org.bitbucket.afx.view.behavior.impl;

import javafx.scene.Node;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.utils.AFXUtils;
import org.bitbucket.afx.view.NodeWrapper;
import org.bitbucket.afx.view.behavior.ViewCondition;
import org.bitbucket.afx.view.validation.ValidationHandler;

public final class ViewConditions {

	private static final Log LOG = LogFactory.getLog(ViewConditions.class);
	
	/**
	 * Applies the given <tt>NodeHandler</tt>, if all values given in <tt>nodesToCheck</tt> are present.
	 * 
	 * @author Martin
	 *
	 */
	public static final class AllValuesProvidedCondition implements ViewCondition {
		
		public boolean meets(Node root, String[] nodesToCheck) {
			boolean result = true;
			if(nodesToCheck != null) {
				for(String id : nodesToCheck) {
					Node node = AFXUtils.findNodeById(root, id);
					if(node == null) {
						LOG.warn("Node with id='" + id + "' does not exist. Assume 'false' as result. Please check your configuration.");
						return false;
					}
					NodeWrapper wrapper = new NodeWrapper(node);
					result &= !AFXUtils.isEmpty(wrapper.getPrimaryValue());
				}
			}
			return result;
		}
	}
	
	/**
	 * Applies the given <tt>NodeHandler</tt>, if at least one value given in <tt>nodesToCheck</tt> is present.
	 * 
	 * @author Martin
	 *
	 */
	public static final class AtLeastOneValueProvidedCondition implements ViewCondition {

		public boolean meets(Node root, String[] nodesToCheck) {
			if(nodesToCheck != null) {
				for(String id : nodesToCheck) {
					Node node = AFXUtils.findNodeById(root, id);
					if(node == null) {
						LOG.warn("Node with id='" + id + "' does not exist. Assume 'false' as result. Please check your configuration.");
						return false;
					}
					NodeWrapper wrapper = new NodeWrapper(node);
					if(!AFXUtils.isEmpty(wrapper.getPrimaryValue())) {
						return true;
					}
				}
			}
			return false;
		}
	}

	/**
	 * Applies the given <tt>NodeHandler</tt>, if at least one value given in <tt>nodesToCheck</tt> is missing.
	 * 
	 * @author Martin
	 *
	 */
	public static final class AtLeastOneValueMissingCondition implements ViewCondition {

		public boolean meets(Node root, String[] nodesToCheck) {
			if(nodesToCheck != null) {
				for(String id : nodesToCheck) {
					Node node = AFXUtils.findNodeById(root, id);
					if(node == null) {
						LOG.warn("Node with id='" + id + "' does not exist. Assume 'false' as result. Please check your configuration.");
						return false;
					}
					NodeWrapper wrapper = new NodeWrapper(node);
					if(AFXUtils.isEmpty(wrapper.getPrimaryValue())) {
						return true;
					}
				}
			}
			return false;
		}
	}

	/**
	 * Applies the given <tt>NodeHandler</tt>, if no values given in <tt>nodesToCheck</tt> are present.
	 * 
	 * @author Martin
	 *
	 */
	public static final class NoValueProvidedCondition implements ViewCondition {

		public boolean meets(Node root, String[] nodesToCheck) {
			boolean result = true;
			if(nodesToCheck != null) {
				for(String id : nodesToCheck) {
					Node node = AFXUtils.findNodeById(root, id);
					if(node == null) {
						LOG.warn("Node with id='" + id + "' does not exist. Assume 'false' as result. Please check your configuration.");
						return false;
					}
					NodeWrapper wrapper = new NodeWrapper(node);
					result &= AFXUtils.isEmpty(wrapper.getPrimaryValue());
				}
			}
			return result;
		}
	}	

	/**
	 * Applies the given <tt>NodeHandler</tt>, if the nodes in <tt>nodesToCheck</tt> have no validation errors.
	 * 
	 * @author Martin
	 *
	 */
	public static final class NoValidationErrorCondition implements ViewCondition {

		private ValidationHandler validationHandler;
		
		/**
		 * Constructor accepting a <tt>ValidationHandler</tt>.
		 * 
		 * @param validationHandler the <tt>ValidationHandler</tt> instance
		 */
		public NoValidationErrorCondition(ValidationHandler validationHandler) {
			this.validationHandler = validationHandler;
		}
		
		public boolean meets(Node root, String[] nodesToCheck) {
			boolean result = true;
			if(nodesToCheck != null) {
				for(String id : nodesToCheck) {
					result &= !this.validationHandler.hasValidationErrors(id);
				}
			}
			return result;
		}
	}
	
	/**
	 * Applies the given <tt>NodeHandler</tt>, if the nodes in <tt>nodesToCheck</tt> have validation errors.
	 * 
	 * @author Martin
	 *
	 */
	public static final class ValidationErrorCondition implements ViewCondition {

		private ValidationHandler validationHandler;
		
		/**
		 * Constructor accepting a <tt>ValidationHandler</tt>.
		 * 
		 * @param validationHandler the <tt>ValidationHandler</tt> instance
		 */
		public ValidationErrorCondition(ValidationHandler validationHandler) {
			this.validationHandler = validationHandler;
		}
		
		public boolean meets(Node root, String[] nodesToCheck) {
			if(nodesToCheck != null) {
				for(String id : nodesToCheck) {
					if(this.validationHandler.hasValidationErrors(id)) {
						return true;
					}
				}
			}
			return false;
		}
	}
	
}
