package org.bitbucket.afx.view.behavior.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.utils.AFXUtils;
import org.bitbucket.afx.view.NodeHandler;
import org.bitbucket.afx.view.behavior.ViewBehavior;
import org.bitbucket.afx.view.behavior.ViewCondition;

import javafx.scene.Node;

/**
 * Implementation of the <tt>ViewBehavior</tt> interface that internally holds a condition checking
 * nodes and a handler that will be invoked, when the condition is met.
 * 
 * @author MartinKoster
 *
 */
public class ConditionBasedViewBehavior implements ViewBehavior {

	private static final Log LOG = LogFactory.getLog(ConditionBasedViewBehavior.class);
	
	private List<String> nodesToCheck = new ArrayList<String>();
	
	private List<String> nodesToHandle = new ArrayList<String>();
	
	private List<ViewCondition> viewConditions = new ArrayList<ViewCondition>();
	
	private List<NodeHandler> nodeHandler = new ArrayList<NodeHandler>();
	
	/**
	 * Adds a <tt>ViewCondition</tt> to this behavior implementation.
	 * 
	 * @param condition the <tt>ViewCondition</tt> to be added to this behavior.
	 * @return this behavior implementation
	 */
	public ConditionBasedViewBehavior addCondition(ViewCondition condition) {
		this.viewConditions.add(condition);
		return this;
	}

	/**
	 * Adds nodes that will be checked by the view condition.
	 * 
	 * @param nodeList a list of nodes
	 * @return this behavior implementation
	 */
	public ConditionBasedViewBehavior addNodesToCheck(String[] nodeList) {
		if(nodeList == null) {
			return this;
		}
		for(String id : nodeList) {
			this.nodesToCheck.add(id);
		}
		return this;
	}

	/**
	 * Adds nodes that will be checked by the view condition.
	 * 
	 * @param node a node 
	 * @return this behavior implementation
	 */
	public ConditionBasedViewBehavior addNodeToCheck(String node) {
		this.nodesToCheck.add(node);
		return this;
	}

	/**
	 * Adds a <tt>NodeHandler</tt> that will be invoked if the <tt>ViewCondition</tt> in this behavior
	 * is met.
	 * 
	 * @param handler the <tt>NodeHandler</tt> to be added to this behavior
	 * @return this behavior implementation
	 */
	public ConditionBasedViewBehavior addNodeHandler(NodeHandler handler) {
		this.nodeHandler.add(handler);
		return this;
	}

	/**
	 * Adds a list of nodes that will be handled by a set <tt>NodeHandler</tt>, in case the
	 * set <tt>ViewCondition</tt> is met.
	 * 
	 * @param nodeList a list of nodes
	 * @return this behavior implementation
	 */
	public ConditionBasedViewBehavior addNodesToHandle(String[] nodeList) {
		if(nodeList == null) {
			return this;
		}
		for(String id : nodeList) {
			this.nodesToHandle.add(id);
		}
		return this;
	}

	/**
	 * A node that will be handled by a set <tt>NodeHandler</tt>, in case the
	 * set <tt>ViewCondition</tt> is met.
	 * 
	 * @param node a node
	 * @return this behavior implementation
	 */
	public ConditionBasedViewBehavior addNodeToHandle(String node) {
		this.nodesToHandle.add(node);
		return this;
	}

	@Override
	public void evaluateAndApply(Node root) {
		if(this.areConditionsMet(root)) {
			List<Node> nodeList = this.retrieveNodeList(root);
			this.applyNodeHandler(root, nodeList);
		}
	}

	/**
	 * Checks, if all added conditions are met.
	 * 
	 * @param root the root node of JavaFX scene graph
	 * @return <tt>true</tt>, if all set <tt>ViewCondition</tt>s are met, <tt>false</tt> otherwise.
	 */
	protected boolean areConditionsMet(Node root) {
		boolean result = true;
		if(this.viewConditions != null && this.viewConditions.size() > 0) {
			for(ViewCondition condition : viewConditions) {
				String[] nodes = new String[nodesToCheck.size()];
				nodes = nodesToCheck.toArray(nodes);
				result &= condition.meets(root, nodes);
				LOG.debug("Condition '" + condition + "' resulted to '" + result + "'");
			}
		}
		
		return result;
	}
	
	/**
	 * Applies all added <tt>NodeHandler</tt>s.
	 * 
	 * @param root the root node of JavaFX scence graph
	 * @param nodeList the list of nodes that shall be handled by the set <tt>NodeHandler</tt> instances
	 */
	protected void applyNodeHandler(Node root, List<Node> nodeList) {
		if(this.nodeHandler != null && this.nodeHandler.size() > 0) {
			for(NodeHandler handler : this.nodeHandler) {
				for(Node node : nodeList) {
					handler.process(node);
				}
			}
		}
	}
	
	/**
	 * Extracts all relevant nodes to handle from the given scene graph 
	 * represented by <tt>root</tt>.
	 * 
	 * @param root the root node of JavaFX scene graph
	 * @return a list of nodes that shall be handled
	 */
	protected List<Node> retrieveNodeList(Node root) {
		List<Node> nodeList = new ArrayList<Node>();
		if(this.nodesToHandle != null && this.nodesToHandle.size() > 0) {
			for(String id : this.nodesToHandle) {
				Node node = AFXUtils.findNodeById(root, id);
				if(node == null) {
					LOG.warn("Node with id='" + id + "' can not be retrieved from the scene graph. Is there a configuration error?");
				}
				nodeList.add(node);
			}
		}
		return nodeList;
	}
}
