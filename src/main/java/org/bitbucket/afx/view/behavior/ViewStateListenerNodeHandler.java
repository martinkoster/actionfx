package org.bitbucket.afx.view.behavior;

import org.bitbucket.afx.view.NodeHandler;
import org.bitbucket.afx.view.NodeWrapper;

import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Node;

/**
 * Implementation of <tt>NodeHandler</tt> that will attached a <tt>ViewStateListener</tt>
 * to nodes in the JavaFX scene graph.
 * 
 * @author MartinKoster
 *
 */
public class ViewStateListenerNodeHandler implements NodeHandler {

	private ViewStateListener listener;

	private Node root;

	/**
	 * Constructor accepting the root node of JavaFX scene graph and the <tt>ViewStateListener</tt>
	 * that will be attached to nodes.
	 * 
	 * @param root the root node of JavaFX scene graph
	 * @param listener the <tt>ViewStateListener</tt> that will be attached to nodes
	 */
	public ViewStateListenerNodeHandler(Node root, ViewStateListener listener) {
		this.root = root;
		this.listener = listener;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void process(Node node) { 
		
		if (NodeWrapper.supportsPrimaryValue(node.getClass())) {
			NodeWrapper wrapper = new NodeWrapper(node);
			if(wrapper.isPrimaryPropertyOfType(ObservableValue.class)) {
				wrapper.getPrimaryObservableValue().addListener(new ViewStateListenerAdapter(root, node, listener));
			}
			if(wrapper.isPrimaryPropertyOfType(ObservableList.class)) {
				wrapper.getPrimaryObservableList().addListener(new ViewStateListenerAdapter(root, node, listener));
			}
			
		}
	}

}
