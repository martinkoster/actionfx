package org.bitbucket.afx.view.fxml;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.view.NodeWrapper;
import org.bitbucket.afx.view.binding.BeanWrapperAdapter;
import org.springframework.util.CollectionUtils;

import javafx.collections.ObservableList;
import javafx.scene.Node;

/**
 * Simple holder object for the model instance, derived from <tt>BeanWrapperAdapter</tt> in order
 * to make use of binding.
 * 
 * @author MartinKoster
 *
 */
public class ModelHolder extends BeanWrapperAdapter {

	private static final Log LOG = LogFactory.getLog(ModelHolder.class);
	
	private Object model;
	
	public ModelHolder(Object model) {
		super(model);
		this.model = model;
	}

	/**
	 * Refreshes bindings. This methods should be called after bean properties have been changed 
	 * and these changes shall be immediately reflected in the UI.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void refreshBindings() {
		this.setBean(this.model);
	}

	/**
	 * Returns the model instance.
	 * @return the model instance
	 */
	public Object getModel() {
		return model;
	}

	/**
	 * Sets the model instance
	 * @param model the model instance to set
	 */
	public void setModel(Object model) {
		this.model = model;
	} 

	/**
	 * Binds the given <tt>path</tt> pointing to a JavaBeans property to a <tt>javafx.beans.property.Property</tt> that is provided by the given <tt>node</tt>.
	 * 
	 * @param path the path pointing to a JavaBeans property
	 * @param node the node used for binding
	 */
	public void bind(String path, Node node) {
		this.bind(path, null, node, null);
	}

	/**
	 * Binds the given <tt>path</tt> pointing to a JavaBeans property to a <tt>javafx.beans.property.Property</tt>, located under <tt>propertyName</tt> in the 
	 * given <tt>node</tt>. 
	 * 
	 * @param path path the path pointing to a JavaBeans property 
	 * @param elementType the type of elements, in case the JavaBeans property is a List, Map, etc.
	 * @param node the node used for binding
	 * @param propertyName the property name that the given <tt>path</tt> shall be bound to
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void bind(String path, Class<?> elementType, Node node, String propertyName) {
		LOG.debug("FxBinding object of type '" + this.model.getClass().getName() + "': Property path '" + path + "' to node '" + node.getClass().getName() + "', using property method '" + path + "'.");

		NodeWrapper nodeWrapper = new NodeWrapper(node);

		String propertyToAccess = propertyName != null ? propertyName : nodeWrapper.getPrimaryValueProperty();
		
		Class type = this.getPropertyType(path);

		if(Collection.class.isAssignableFrom(type)) {

			Class contentType = null;
			if(elementType != null && !Object.class.equals(elementType)) {
				contentType = elementType;
			} else {
				contentType = CollectionUtils.findCommonElementType((Collection<?>) this.getPropertyValue(path));
			}
			
			ObservableList items = nodeWrapper.getObservableList(propertyToAccess);
			this.bindContentBidirectional(path, null, null,
					items, contentType, null, null);
			
		} else {
			this.bindBidirectional(path, nodeWrapper.getProperty(propertyToAccess), type);
		}

	}
}
