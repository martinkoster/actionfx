package org.bitbucket.afx.view.fxml;

import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * Implementation of <code>AbstractFxmlDatabindingView</code> for views that are loaded from a <tt>fxml</tt> 
 * file and that represent a main window.
 * 
 * @author MartinKoster
 *
 */
public class FxmlWindowView extends AbstractFxmlDatabindingView {
	
	private Scene scene;
	
	@Override
	public void showAfterBind() {
		
    	this.getPrimaryStage().setScene(this.createOrGetScene());
	    if(this.getTitle() != null && !"".equals(this.getTitle())) {
	    	this.getPrimaryStage().setTitle(this.getTitle());
	    }
	    this.getPrimaryStage().setMaximized(this.isMaximized());
	    this.getPrimaryStage().show();		
	    
		if(this.hasErrorOccured()) {
			this.displayThrowable();
			this.resetError();
		}
	}
	
	/**
	 * Creates a scene object, if not already constructed. If there is a scene instance already, it will
	 * be simply returned.
	 * 
	 * @return the <tt>javafx.scene.Scene</tt> instance
	 */
	public Scene createOrGetScene() {
		if(this.scene != null) {
			return this.scene;
		}
		if(this.getSizeX() != -1 && this.getSizeY() != -1) {
			this.scene = new Scene((Parent) this.getNode(), this.getSizeX(), this.getSizeY());
	    } else {
	    	this.scene = new Scene((Parent) this.getNode());
	    }
		return this.scene;
	}
}
