package org.bitbucket.afx.view;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.i18.exception.ErrorCodeSupportingException;
import org.bitbucket.afx.view.validation.ValidationException;

/**
 * Abstract view class that adds error handling in the view, e.g. for displaying an exception.
 * 
 * @author MartinKoster
 *
 */
public abstract class AbstractErrorHandlingView extends AbstractView {

	private static final Log LOG = LogFactory.getLog(AbstractErrorHandlingView.class);
	
	private Throwable throwable;

	/**
	 * Returns the <tt>java.lang.Throwable</tt> associated with this view.
	 * @return the <tt>java.lang.Throwable</tt> associated with this view
	 */
	public Throwable getThrowable() {
		return throwable;
	}

	/**
	 * Sets the <tt>Throwable</tt> that shall be handled by this view.
	 * 
	 * @param throwable the <tt>java.lang.Throwable</tt> that the view shall be associated with
	 */
	public void setThrowable(Throwable throwable) {
		LOG.debug("Setting exception of type '" + throwable.getClass().getName() + "' into view '" + this.getClass().getName()  + "'.");
		this.throwable = throwable;
	}

	/**
	 * Displays the exception in a separate modal dialogue.
	 */
	public void displayThrowable() {
		if(this.throwable instanceof ValidationException) {
			this.getValidationHandler().showValidationErrorDialog((ValidationException) this.throwable);
		} else if(this.throwable instanceof ErrorCodeSupportingException) {
			ErrorCodeSupportingException exc = (ErrorCodeSupportingException) this.throwable;
			String title = this.getMessage("message_error_dialog");
			String  message = this.getMessage(exc.getErrorCode());
			DialogUtils.showErrorDialog(title, message);
		} else {
			LOG.error(this.throwable.getMessage(), this.throwable);
			String title = this.getMessage("message_exception_dialog");
			String message = this.getMessage("message_exception_text");
			DialogUtils.showExceptionDialog(title, message, throwable);
		}
	}
	
	/**
	 * Returns <tt>true</tt> whether there is a <tt>java.lang.Throwable</tt> associated with this view.
	 * 
	 * @return <tt>true</tt>, wether there is a <tt>java.lang.Throwable</tt> associated with this view
	 */
	public boolean hasErrorOccured() {
		return this.throwable != null;
	}
	
	/**
	 * Resets the view and removes the error.
	 */
	public void resetError() {
		this.throwable = null;
	}
}
