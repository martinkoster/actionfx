package org.bitbucket.afx.view.validation;

import javafx.scene.Node;

/**
 * Validation item holding a single validation error.
 * 
 * @author MartinKoster
 *
 */
public class ValidationItem  {

	private String message;
	
	private Node affectedNode;

	/**
	 * Constructor accepting a message and the affected node.
	 * 
	 * @param message the message
	 * 
	 * @param affectedNode the node that is affected by the validation error
	 */
	public ValidationItem(String message, Node affectedNode) {
		this.message = message;
		this.affectedNode = affectedNode;
	}
	
	/**
	 * Constructor accepting a single message.
	 * 
	 * @param message the message
	 */
	public ValidationItem(String message) {
		this(message, null);
	}

	/**
	 * Returns the node that is affected by the validation error.
	 * 
	 * @return the node that is affected by the validation error
	 */
	public Node getAffectedNode() {
		return affectedNode;
	}

	/**
	 * Sets the node that is affected by the validation error
	 * 
	 * @param affectedNode the node that is affected by the validation error
	 */
	public void setAffectedNode(Node affectedNode) {
		this.affectedNode = affectedNode;
	}

	/**
	 * Returns the message for this validation error.
	 * 
	 * @return the message for this validation error
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message for this validation error.
	 * 
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}	
}
