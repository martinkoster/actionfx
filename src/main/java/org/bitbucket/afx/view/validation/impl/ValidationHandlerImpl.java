package org.bitbucket.afx.view.validation.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javafx.scene.Node;
import javafx.scene.control.Control;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.AFXMessageSource;
import org.bitbucket.afx.annotation.FxRequiredValidation;
import org.bitbucket.afx.annotation.FxRequiredValidations;
import org.bitbucket.afx.utils.AFXUtils;
import org.bitbucket.afx.utils.AnnotationUtils;
import org.bitbucket.afx.view.DialogUtils;
import org.bitbucket.afx.view.validation.ValidationException;
import org.bitbucket.afx.view.validation.ValidationHandler;
import org.bitbucket.afx.view.validation.ValidationItem;
import org.controlsfx.validation.ValidationMessage;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.CompoundValidationDecoration;
import org.controlsfx.validation.decoration.GraphicValidationDecoration;
import org.controlsfx.validation.decoration.StyleClassValidationDecoration;
import org.controlsfx.validation.decoration.ValidationDecoration;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;

/**
 * Implementation of the <tt>ValidationHandler</tt> interface, making use of the ControlsFX validation framework.
 * 
 * @author MartinKoster
 *
 */
public class ValidationHandlerImpl implements ValidationHandler {

	private static final Log LOG = LogFactory.getLog(ValidationHandlerImpl.class);

	private ValidationSupport validationSupport;

	private Object modelHolder;

	private Node root;

	private AFXMessageSource messageSource;

	/**
	 * Constructor that accepts the model as <tt>modelHolder</tt>, the root node of JavaFX scene graph and
	 * a <tt>AFXMessageSource</tt> for looking up internationalized texts.
	 * 
	 * @param modelHolder the model instance
	 * @param root the root node of JavaFX scene graph
	 * @param messageSource a <tt>AFXMessageSource</tt> for looking up internationalized texts
	 */
	public ValidationHandlerImpl(final Object modelHolder, final Node root, final AFXMessageSource messageSource) {
		this.modelHolder = modelHolder;
		this.root = root;
		this.messageSource = messageSource;
		ValidationDecoration iconDecorator = new GraphicValidationDecoration();
		ValidationDecoration cssDecorator = new StyleClassValidationDecoration();
		ValidationDecoration compoundDecorator = new CompoundValidationDecoration(cssDecorator, iconDecorator);
		this.validationSupport = new ValidationSupport();
		this.validationSupport.setValidationDecorator(compoundDecorator);
		this.registerValidators();
	}

	/**
	 * Registers all validators defined in the controller via annotations in the
	 * view nodes.
	 * 
	 */
	protected void registerValidators() {
		ValidatorRegistrar registrar = new ValidatorRegistrar(root, messageSource);
		ReflectionUtils.doWithFields(this.modelHolder.getClass(), registrar);
	}

	@Override
	public boolean hasValidationErrors() {
		ValidationResult result = this.validationSupport.validationResultProperty().get();
		return result == null ? false : result.getErrors() != null && result.getErrors().size() > 0;
	}

	@Override
	public boolean hasValidationErrors(String nodeId) {
		if (this.hasValidationErrors()) {
			ValidationResult result = this.validationSupport.validationResultProperty().get();
			for (ValidationMessage validationMessage : result.getMessages()) {
				
				if (validationMessage.getTarget() != null && validationMessage.getTarget().getId() != null
						&& validationMessage.getTarget().getId().equals(nodeId)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void throwValidationExceptionIfHasErrors() throws ValidationException {
		if (this.hasValidationErrors()) {
			throw new ValidationException(this.getValidationItems());
		}
	}

	@Override
	public List<String> getValidationErrorMessages() {
		ValidationResult result = this.validationSupport.validationResultProperty().get();
		List<String> messages = new ArrayList<String>();
		if (this.hasValidationErrors()) {
			for (ValidationMessage validationMessage : result.getMessages()) {
				messages.add(validationMessage.getText());
			}
		}
		return messages;
	}

	@Override
	public List<ValidationItem> getValidationItems() {
		ValidationResult result = this.validationSupport.validationResultProperty().get();
		List<ValidationItem> items = new ArrayList<ValidationItem>();
		for (ValidationMessage validationMessage : result.getMessages()) {
			items.add(new ValidationItem(validationMessage.getText(), validationMessage.getTarget()));
		}
		return items;
	}

	@Override
	public String getValidationErrorMessagesForDisplay() {
		StringBuilder b = new StringBuilder();
		for (String message : this.getValidationErrorMessages()) {
			b.append(message).append("\n");
		}
		return b.toString();
	}

	@Override
	public String getValidationErrorMessagesForDisplay(ValidationException exception) {
		StringBuilder b = new StringBuilder();
		for (ValidationItem item : exception.getValidationItems()) {
			b.append(item.getMessage()).append("\n");
		}
		return b.toString();
	}

	@Override
	public void showValidationErrorDialog() {
		String title = this.messageSource.getMessage("message_validation_dialog", null);
		DialogUtils.showErrorDialog(title, this.getValidationErrorMessagesForDisplay());
	}

	@Override
	public void showValidationErrorDialog(ValidationException exception) {
		String title = this.messageSource.getMessage("message_validation_dialog", null);
		DialogUtils.showErrorDialog(title, this.getValidationErrorMessagesForDisplay(exception));
	}

	/**
	 * Field-level validator that is validating the field against the applied
	 * validations.
	 * 
	 * @author MartinKoster
	 *
	 */
	private class ValidatorRegistrar implements FieldCallback {

		private Node root;

		private AFXMessageSource messageSource;

		/**
		 * Constructor accepting the root node of JavaFX scene graph and a <tt>AFXMessageSource</tt> for looking up internationalized texts.
		 * 
		 * @param root the root node of JavaFX scene graph
		 * @param messageSource a <tt>AFXMessageSource</tt> for looking up internationalized texts
		 */
		public ValidatorRegistrar(Node root, AFXMessageSource messageSource) {
			this.root = root;
			this.messageSource = messageSource;
		}

		@Override
		public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
			this.registerNotEmptyValidators(field);
		}

		/**
		 * Validate, whether mandatory fields are not empty.
		 * 
		 * @param field the field to register the validator with
		 */
		protected void registerNotEmptyValidators(Field field) {
			Set<FxRequiredValidation> annotations = AnnotationUtils.getRepeatableAnnotation(field,
					FxRequiredValidations.class, FxRequiredValidation.class);
			if (annotations == null || annotations.size() == 0) {
				return;
			}

			for (FxRequiredValidation annotation : annotations) {
				String nodeId = this.getLookupNodeId(field, annotation.path());

				// register validator
				Node node = AFXUtils.findNodeById(root, nodeId);
				if (node instanceof Control) {
					LOG.debug("Applying NotEmptyValidator to node with id='" + nodeId + "', message code is '"
							+ annotation.code() + "'");
					validationSupport.registerValidator((Control) node,
							Validator.createEmptyValidator(this.getMessage(annotation.code())));
				}
			}
		}

		/**
		 * Determines the node ID that needs to be checked for the given
		 * <tt>field</tt> and the supplied <tt>path</tt>.
		 * 
		 * @param field the <tt>java.lang.reflect.Field</tt> for that the corresponding node in the scene graph shall be determined
		 * @param path the path to the node in the scene graph
		 * @return the node ID that that path points at
		 */
		protected String getLookupNodeId(Field field, String path) {
			// construct nested path to lookup values relative to the annotated
			// field
			String nodeId = (StringUtils.trimToNull(path) == null) ? field.getName() : field.getName() + "." + path;
			return nodeId;
		}

		/**
		 * Gets a message located under a given <tt>code</tt>.
		 * 
		 * @param code the code to use for looking up the message 
		 * @return the retrieved message, or <tt>null</tt>, in case the message can not be retrieved.
		 */
		public String getMessage(String code) {
			return this.getMessageSource().getMessage(code, null);
		}

		/**
		 * Returns the set <tt>AFXMessageSource</tt>.
		 * @return the set <tt>AFXMessageSource</tt>
		 */
		public AFXMessageSource getMessageSource() {
			return messageSource;
		}
	}

}
