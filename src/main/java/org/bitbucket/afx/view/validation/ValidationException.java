package org.bitbucket.afx.view.validation;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.Node;

/**
 * Validation exception class that carries one or more <code>ValidationItem</code>s.
 * 
 * @author MartinKoster
 *
 */
public class ValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<ValidationItem> validationItems = new ArrayList<ValidationItem>();
	
	/**
	 * Constructor accepting a list of <tt>ValidationItem</tt>s.
	 * 
	 * @param items the list of <tt>ValidationItems</tt>
	 */
	public ValidationException(List<ValidationItem> items) {
		this.validationItems.addAll(items);
	}
	
	/**
	 * Constructor accepting a single <tt>ValidationItem</tt>.
	 * 
	 * @param item the <tt>ValidationItem</tt> to be set
	 */
	public ValidationException(ValidationItem item) {
		this.validationItems.add(item);
	}
	
	/**
	 * Constructor accepting a message and an affected node.
	 * 
	 * @param message the message to set
	 * @param affectedNode the node that is affected by a validation error
	 */
	public ValidationException(String message, Node affectedNode) {
		this(new ValidationItem(message, affectedNode));
	}

	/**
	 * Constructor accepting a message. It is not possible to determine, which node
	 * triggered this <tt>ValidationException</tt>.
	 * 
	 * @param message
	 */
	public ValidationException(String message) {
		this(message, null);
	}	

	/**
	 * Returns the list of <tt>ValidationItem</tt>s.
	 * 
	 * @return the list of <tt>ValidationItem</tt>s
	 */
	public List<ValidationItem> getValidationItems() {
		return validationItems;
	}

	/**
	 * Sets a list of <tt>ValidationItem</tt>s.
	 * 
	 * @param validationItems the list of <tt>ValidationItem</tt>s to set
	 */
	public void setValidationItems(List<ValidationItem> validationItems) {
		this.validationItems = validationItems;
	}
	
}
