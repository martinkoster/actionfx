package org.bitbucket.afx.view.validation;

import java.util.List;

/**
 * Interface for validation-specific handling with one view.
 * 
 * @author MartinKoster
 *
 */
public interface ValidationHandler {

	/**
	 * Returns <tt>true</tt>, if there are validation errors, <tt>false</tt> otherwise.
	 *  
	 * @return <tt>true</tt>, if there are validation errors, <tt>false</tt> otherwise
	 */
	public boolean hasValidationErrors();

	/**
	 * Returns <tt>true</tt>, if there are validation errors for the node identified by <tt>nodeId</tt>, <tt>false</tt> otherwise.
	 *  
	 * @return <tt>true</tt>, if there are validation errors for the node identified by <tt>nodeId</tt>, <tt>false</tt> otherwise
	 */
	public boolean hasValidationErrors(String nodeId);
	
	/**
	 * Constructs and throws an instance of <tt>ValidationException</tt> in case there are validation errors.
	 */
	public void throwValidationExceptionIfHasErrors() throws ValidationException;	
	
	/**
	 * Returns the list of validation messages.
	 * 
	 * @return a list of validation error messages as plaing Strings.
	 */
	public List<String> getValidationErrorMessages();

	/**
	 * Returns the list of <tt>ValidationItem</tt>s.
	 * 
	 * @return a list of <tt>ValidationItem</tt>s
	 */
	public List<ValidationItem> getValidationItems();
	
	/**
	 * Returns all validation messages combined into one string for display.
	 * 
	 * @return all validation messages combined into one string for display
	 */
	public String getValidationErrorMessagesForDisplay();
	/**
	 * Returns a validation message for the given <tt>ValidationException</tt> combined into one string for display.
	 * 
	 * @return a validation message for the given <tt>ValidationException</tt> combined into one string for display
	 */
	public String getValidationErrorMessagesForDisplay(ValidationException exception);
	
	/**
	 * Displays a modal error dialog containing the validation errors associated with the instance 
	 * of <tt>ValidationSupport</tt>.
	 */
	public void showValidationErrorDialog();

	/**
	 * Displays a modal error dialog containing the validation errors.
	 * 
	 * @param exception the <tt>ValidationException</tt> to be displayed in the error dialog.
	 */
	public void showValidationErrorDialog(ValidationException exception);
}
