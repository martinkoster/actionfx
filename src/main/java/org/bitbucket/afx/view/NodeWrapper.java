package org.bitbucket.afx.view;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bitbucket.afx.utils.ReflectiveAccessor;
import org.controlsfx.control.StatusBar;

import javafx.beans.property.Property;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Accordion;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;

/**
 * Node-wrapper to abstract the different node types of JavaFX and to provide a unique access to it.
 * 
 * @author Martin
 *
 */
public class NodeWrapper extends ReflectiveAccessor {

	public static final String PROP_TEXTPROPERTY = "textProperty";
	public static final String PROP_MESSAGEPROPERTY = "messageProperty";
	public static final String PROP_SELECTEDPROPERTY = "selectedProperty";
	public static final String PROP_PROGRESSPROPERTY = "progressProperty";
	public static final String PROP_GETITEMS = "getItems";
	
	private static Map<Class<? extends Node>, String> PRIMARY_VALUE_PROPERTY = null;
	
	static {
		
		PRIMARY_VALUE_PROPERTY = new HashMap<Class<? extends Node>, String>();
		PRIMARY_VALUE_PROPERTY.put(PasswordField.class, PROP_TEXTPROPERTY);
		PRIMARY_VALUE_PROPERTY.put(TextField.class, PROP_TEXTPROPERTY);
		PRIMARY_VALUE_PROPERTY.put(ListView.class, PROP_GETITEMS);
		PRIMARY_VALUE_PROPERTY.put(CheckBox.class, PROP_SELECTEDPROPERTY);
		PRIMARY_VALUE_PROPERTY.put(ComboBox.class, PROP_GETITEMS);
		PRIMARY_VALUE_PROPERTY.put(TextArea.class, PROP_TEXTPROPERTY);
		PRIMARY_VALUE_PROPERTY.put(RadioButton.class, PROP_SELECTEDPROPERTY);
		PRIMARY_VALUE_PROPERTY.put(ProgressBar.class, PROP_PROGRESSPROPERTY);
		PRIMARY_VALUE_PROPERTY.put(StatusBar.class, PROP_PROGRESSPROPERTY);
		PRIMARY_VALUE_PROPERTY.put(Label.class, PROP_TEXTPROPERTY);
		PRIMARY_VALUE_PROPERTY.put(TableView.class, PROP_GETITEMS);
	}
	
	private String primaryValueProperty;
	
	private Node node;
	
	/**
	 * Constructor accepting a <code>javafx.scene.Node</code> instance for wrapping.
	 * @param node the node to be wrapped
	 */
	public NodeWrapper(Node node) {
		super(node);
		this.node = node;
		this.primaryValueProperty = PRIMARY_VALUE_PROPERTY.get(node.getClass());
	}

	/**
	 * Gets the primary value from this node. The primary value is available for a predefined set of components.
	 * <p>
	 * The following components are supported with the specified primary value property:
	 * <p>
	 * <ul>
	 *  <li>javafx.scene.control.TextField -> textProperty()</li>
	 *  <li>javafx.scene.control.PasswordField -> textProperty()</li>
	 *  <li>javafx.scene.control.ListView -> getItems()</li>
	 *  <li>javafx.scene.control.CheckBox -> selectedProperty()</li>
	 *  <li>javafx.scene.control.ComboBox -> getItems()</li>
	 *  <li>javafx.scene.control.TextArea -> textProperty()</li>
	 *  <li>javafx.scene.control.RadioButton -> selectedProperty()</li>
	 *  <li>javafx.scene.control.ProgressBar -> progressProperty()</li>
	 *  <li>org.controlsfx.control.StatusBar -> progressProperty()</li>
	 *  <li>javafx.scene.control.Label -> textProperty()</li>
	 *  <li>javafx.scene.control.TableView -> getItems()</li>  
	 * </ul>
	 * 
	 * @return the value stored in the primary property
	 */
	public Object getPrimaryValue() {
		if(checkMethodReturnType(this.primaryValueProperty, this.node.getClass(), ObservableValue.class)) {
			return this.getPrimaryObservableValue().getValue();
		}
		if(checkMethodReturnType(this.primaryValueProperty, this.node.getClass(), ObservableList.class)) {
			return this.getPrimaryObservableList();
		}
		return null;		
	}

	/**
	 * Gets the <tt>javafx.beans.value.ObservableValue</tt> instance from the primary property.
	 * <p>
	 * See also: {@link org.bitbucket.afx.view.NodeWrapper#getPrimaryValue}  
	 * @return the <tt>javafx.beans.value.ObservableValue</tt> instance from the primary property
	 */
	public ObservableValue<?> getPrimaryObservableValue() {
		return this.getObservableValue(this.primaryValueProperty);
	}

	/**
	 * Gets the <tt>javafx.beans.property.Property</tt> instance from the primary property.
	 * <p>
	 * See also: {@link org.bitbucket.afx.view.NodeWrapper#getPrimaryValue}  
	 * @return the <tt>javafx.beans.property.Property</tt> instance from the primary property
	 */
	public Property<?> getPrimaryProperty() {
		return this.getProperty(this.primaryValueProperty);
	}

	/**
	 * Gets the <tt>javafx.collections.ObservableList</tt> instance from the primary property.
	 * <p>
	 * See also: {@link org.bitbucket.afx.view.NodeWrapper#getPrimaryValue}  
	 * @return the <tt>javafx.collections.ObservableList</tt> instance from the primary property
	 */
	public ObservableList<?> getPrimaryObservableList() {
		return this.getObservableList(this.primaryValueProperty);
	}
	
	/**
	 * Gets the value located under the specified <tt>propertyName</tt>.
	 * 
     * @param propertyName the name of the property
	 * @return the value located under the specified <tt>propertyName</tt>
	 */
	public Object getValue(String propertyName) {
		return this.getObservableValue(propertyName).getValue();
	}
	
	/**
	 * Gets the <tt>javafx.beans.value.ObservableValue</tt> instance located under the specified <tt>propertyName</tt>.
	 * 
	 * @param propertyName the name of the property
	 * @return the <tt>javafx.beans.value.ObservableValue</tt> instance 
	 */
	public ObservableValue<?> getObservableValue(String propertyName) {
		return this.getPropertyValue(propertyName, ObservableValue.class);
	}
	
	/**
	 * Gets the <tt>javafx.beans.property.Property</tt> instance located under the specified <tt>propertyName</tt>.
	 * 
	 * @param propertyName the name of the property
	 * @return the <tt>javafx.beans.property.Property</tt> instance 
	 */
	public Property<?> getProperty(String propertyName) {
		return this.getPropertyValue(propertyName, Property.class);
	}
	
	/**
	 * Gets the <tt>javafx.collections.ObservableList</tt> instance located under the specified <tt>propertyName</tt>.
	 * 
	 * @param propertyName the name of the property
	 * @return the <tt>javafx.collections.ObservableList</tt> instance
	 */
	public ObservableList<?> getObservableList(String propertyName) {
		return this.getPropertyValue(propertyName, ObservableList.class);
	}
	
	/**
	 * Checks, whether the property defined by <tt>propertyName</tt> supports the given <tt>type</tt>, i.e. it will be checked whether
	 * the property under the path is of that type.
	 * 
	 * @param propertyName the name of the property
	 * @param type the type to be checked
	 * @return <tt>true</tt>, if the property is of the specified <tt>type</tt>, <tt>false</tt> otherwise.
	 */
	public boolean isPropertyOfType(String propertyName, Class<?> type) {
		return checkMethodReturnType(propertyName, this.getWrappedInstance().getClass(), type);
	}

	/**
	 * Checks, whether the default property supports the given <tt>type</tt>, i.e. it will be checked whether
	 * the property under the path is of that type.
	 * <p>
	 * See also: {@link org.bitbucket.afx.view.NodeWrapper#getPrimaryValue}
	 * 
	 * @param type the type to be checked
	 * @return <tt>true</tt>, if the primary property is of the specified <tt>type</tt>, <tt>false</tt> otherwise.
	 */
	public boolean isPrimaryPropertyOfType(Class<?> type) {
		if(this.primaryValueProperty == null) {
			return false;
		}
		return checkMethodReturnType(this.primaryValueProperty, this.getWrappedInstance().getClass(), type);
	}
	

	/**
	 * Returns the name of the primary value property.
	 * <p>
	 * See also: {@link org.bitbucket.afx.view.NodeWrapper#getPrimaryValue}
	 * 
	 * @return the value stored under the primary property
	 */
	public String getPrimaryValueProperty() {
		return primaryValueProperty;
	}

	/**
	 * Traverses the JavaFX scene graph represented by this <code>NodeWrapper</code>. 
	 * Nodes are processed by applying the given <code>nodeHandler</code>.
	 * 
	 * @param nodeHandler the <tt>NodeHandler</tt> to be invoked during node traversal
	 */
	
	public void traverse(NodeHandler nodeHandler) {
		traverse(this.node, nodeHandler);
	}
	
	/**
	 * Traverses the JavaFX scene graph represented by the node <code>parent</code>. 
	 * Nodes are processed by applying the given <code>nodeHandler</code>.
	 * 
	 * @param parent the node to start the scene graph traversal from
	 * @param nodeHandler the <tt>NodeHandler</tt> to be invoked during node traversal
	 */
	public static void traverse(Node parent, NodeHandler nodeHandler) {
		
		// process node... 
		nodeHandler.process(parent);

		// continue processing?
		if(!nodeHandler.continueTraversing()) {
			return;
		}
		
		// get child nodes and traverse on each node
		List<Node> childNodes = getChildren(parent);
		for(Node childNode : childNodes) {
			if(nodeHandler.continueTraversing()) {
				traverse(childNode, nodeHandler);
			} else {
				break;
			}
		}
	}	

	/**
	 * Return the children of this node.
	 * 
	 * @return the children of this node
	 */
	public List<Node> getChildren() {
		return getChildren(this.node);
	}
	
	/**
	 * Return the children of the given node <tt>parent</tt>.
	 * 
	 * @param parent the node to be checked for children
	 * @return the children of the given <tt>parent</tt>
	 */
	public static List<Node> getChildren(Node parent) {
		List<Node> childNodes = new ArrayList<Node>();
		
		if(parent instanceof Parent) {
			
			// certain components have different ways to get the child nodes
			if (parent instanceof TitledPane) {
				TitledPane titledPane = (TitledPane) parent;
				childNodes.add(titledPane.getContent());
			} else if (parent instanceof ScrollPane) {
				ScrollPane scrollPane = (ScrollPane) parent;
				childNodes.add(scrollPane.getContent());
			} else if (parent instanceof SplitPane) {
				SplitPane splitPane = (SplitPane) parent;
				childNodes.addAll(splitPane.getItems());
			} else if (parent instanceof Accordion) {
				Accordion accordion = (Accordion) parent;
				childNodes.addAll(accordion.getPanes());
			} else {
				Parent parentNode = (Parent) parent;
				childNodes.addAll(parentNode.getChildrenUnmodifiable());
			}
		}
		return childNodes;
	}

	/**
	 * Checks, if the given <tt>Node</tt> class has a primary value, i.e. 
	 * a central main value, like "textProperty" for <tt>Label</tt>, etc..
	 * 
	 * @param clazz the class to be checked, whether it is supported or not
	 * @return <tt>true</tt>, if the given <tt>clazz</tt> is supported, <tt>false</tt> otherwise.
	 */
	public static boolean supportsPrimaryValue(Class<? extends Node> clazz) {
		return PRIMARY_VALUE_PROPERTY.containsKey(clazz);
	}
	
	/**
	 * Gets the node wrapped by this instance.
	 * 
	 * @return the wrapped node
	 */
	public Node getNode() {
		return node;
	}

}


