package org.bitbucket.afx.view;

import javafx.scene.Node;

/**
 * Callback interface for handling nodes during JavaFX scene graph traversal.
 * 
 * @author MartinKoster
 *
 */
public interface NodeHandler {

	/**
	 * Method for processing a <code>javafx.scene.Node</code> during scene graph traversal.
	 * 
	 * @param node the node to process
	 */
	public void process(Node node);

	/**
	 * Implementations must return <tt>true</tt>, if traversing the scene graph shall be continued. If <tt>false</tt> is
	 * returned, traversing will be stopped.
	 * 
	 * @return <tt>true</tt>, if the scene graph traversal shall be continued, <tt>false</tt>, if it shall be stopped.
	 */
	default public boolean continueTraversing() { return true; }
}
