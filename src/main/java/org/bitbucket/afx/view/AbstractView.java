package org.bitbucket.afx.view;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.afx.AFXComponentCache;
import org.bitbucket.afx.AFXMessageSource;
import org.bitbucket.afx.AFXPrimaryStageHolder;
import org.bitbucket.afx.view.behavior.ViewStateListener;
import org.bitbucket.afx.view.behavior.ViewStateListenerNodeHandler;
import org.bitbucket.afx.view.behavior.ViewStateManager;
import org.bitbucket.afx.view.behavior.impl.ViewStateManagerImpl;
import org.bitbucket.afx.view.validation.ValidationHandler;

import javafx.scene.Node;
import javafx.stage.Stage;

/**
 * Abstract class implementation for the <code>View</code> interface.
 * 
 * Implementing classes are responsible for constructing the JavaFX object
 * hierarchy of the <tt>Node</tt> instance.
 * 
 * @author MartinKoster
 *
 */
public abstract class AbstractView implements View {

	private String id;

	private Node node;

	private boolean maximized = false;

	private int sizeX = -1;

	private int sizeY = -1;

	private boolean modal = false;

	private String title = "";

	private AFXComponentCache componentCache;

	private AFXPrimaryStageHolder primaryStageHolder;

	private AFXMessageSource messageSource;

	private ValidationHandler validationHandler;

	private List<ViewStateListener> viewStateListener;

	private ViewStateManager viewStateManager = new ViewStateManagerImpl(this);

	/**
	 * Returns the ID of this view.
	 * @return the ID of this view
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the ID of this view.
	 * 
	 * @param id the ID of this view
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Returns the root node of the JavaFX scene graph.
	 * 
	 * @return the root node of the JavaFX scene graph
	 */
	public Node getNode() {
		return node;
	}

	/**
	 * Sets the root node containing the JavaFX scene graph. In case there are <tt>ViewStateListener</tt>s registered
	 * to the view already, the listeners will be connected to the nodes in the given scene graph, so that the
	 * listener will receive generated view change events.
	 * 
	 * @param node the root node of the JavaFX scence graph
	 */
	public void setNode(Node node) {
		this.node = node;
		if (this.node != null) {

			if (this.viewStateListener != null) {

				for (ViewStateListener listener : this.viewStateListener) {
					this.attachListenerToNodes(listener);
				}

			}
		}
	}

	/**
	 * Returns the primary <tt>javafx.stage.Stage</tt> that is constructed during JavaFX startup.
	 * 
	 * @return the primary <tt>javafx.stage.Stage</tt>
	 */
	public Stage getPrimaryStage() {
		return this.getPrimaryStageHolder() != null ? this.getPrimaryStageHolder().getPrimaryStage() : null;
	}

	/**
	 * Returns <tt>true</tt>, if this will shall be displayed maximized.
	 * 
	 * @return <tt>true</tt>, if this view shall be displayed maximized, <tt>false</tt> otherwise
	 */
	public boolean isMaximized() {
		return maximized;
	}

	/**
	 * Setter for specifying whether this view shall be maximized or not.
	 *  
	 * @param maximized <tt>true</tt>, if this view shall be displayed maximized, <tt>false</tt> otherwise
	 */
	public void setMaximized(boolean maximized) {
		this.maximized = maximized;
	}

	/**
	 * Gets the width of this view.
	 * 
	 * @return the width of this view
	 */
	public int getSizeX() {
		return sizeX;
	}

	/**
	 * Sets the width of this view.
	 * 
	 * @param sizeX the width of this view
	 */
	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}

	/**
	 * Gets the height of this view.
	 * 
	 * @return the height of this view
	 */
	public int getSizeY() {
		return sizeY;
	}

	/**
	 * Sets the height of this view.
	 * 
	 * @param sizeY the height of this view 
	 */
	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}

	/**
	 * Returns <tt>true</tt>, if this view is a modal dialog, <tt>false</tt> otherwise.
	 * 
	 * @return <tt>true</tt>, if this view is a modal dialog, <tt>false</tt> otherwise
	 */
	public boolean isModal() {
		return modal;
	}

	/**
	 * Setter for specifying whether this view shall be modal or not.
	 * 
	 * @param modal <tt>true</tt>, if this view is a modal dialog, <tt>false</tt> otherwise
	 */
	public void setModal(boolean modal) {
		this.modal = modal;
	}

	/**
	 * Gets the title of this view (or window title).
	 * 
	 * @return the title of this view / window
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title of this view (or window title).
	 * 
	 * @param title the title of this view / window
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the <tt>AFXComponentCache</tt>.
	 * 
	 * @return the <tt>AFXComponentCache</tt>
	 */
	public AFXComponentCache getComponentCache() {
		return componentCache;
	}

	/**
	 * Sets the <tt>AFXComponentCache</tt>.
	 * @param componentCache the <tt>AFXComponentCache</tt>
	 */
	public void setComponentCache(AFXComponentCache componentCache) {
		this.componentCache = componentCache;
	}

	/**
	 * Returns the primary stage holder, which is an instance providing access to the
	 * primary <tt>javafx.stage.Stage</tt> constructed by JavaFX on startup.
	 * 
	 * @return the <tt>PrimaryStageHolder</tt>
	 */
	public AFXPrimaryStageHolder getPrimaryStageHolder() {
		return primaryStageHolder;
	}

	/**
	 * Sets the primary stage holder.
	 * 
	 * @param primaryStageHolder the <tt>PrimaryStageHolder</tt> to set
	 */
	public void setPrimaryStageHolder(AFXPrimaryStageHolder primaryStageHolder) {
		this.primaryStageHolder = primaryStageHolder;
	}

	/**
	 * Returns the <tt>AFXMessageSource</tt> that provides support for internationalization.
	 * 
	 * @return the <tt>AFXMessageSource</tt>
	 */
	public AFXMessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Sets the <tt>AFXMessageSource</tt>.
	 * 
	 * @param messageSource the <tt>AFXMessageSource</tt> to set
	 */
	public void setMessageSource(AFXMessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * Retrieves a message from the set <tt>AFXMessageSource</tt>.
	 * 
	 * @param code the code that shall be resolved 
	 * @return the message found under the given <tt>code</tt>, or <tt>null</tt>, if the code could not be resolved
	 */
	public String getMessage(String code) {
		return this.getMessageSource().getMessage(code, null);
	}

	/**
	 * Retrieves a message from the set <tt>AFXMessageSource</tt>.
	 * 
	 * @param code the code that shall be resolved 
	 * @param args an array of arguments that shall be woven into the resolved message
	 * @return the message found under the given <tt>code</tt>, or <tt>null</tt>, if the code could not be resolved
	 */
	public String getMessage(String code, Object[] args) {
		return this.getMessageSource().getMessage(code, args);
	}

	/**
	 * Returns the responsible <tt>ValidationHandler</tt> that is performing validations
	 * on form fields within this view.
	 * 
	 * @return the <tt>ValidationHandler</tt> that is set to this view
	 */
	public ValidationHandler getValidationHandler() {
		return validationHandler;
	}

	/**
	 * Sets a <tt>ValidationHandler</tt> to this view instance.
	 * 
	 * @param validationHandler a <tt>ValidationHandler</tt> to set
	 */
	public void setValidationHandler(ValidationHandler validationHandler) {
		this.validationHandler = validationHandler;
	}

	@Override
	public void addListener(ViewStateListener listener) {
		if (listener == null) {
			throw new IllegalArgumentException("Argument 'listener' must not be null!");
		}
		if (this.viewStateListener == null) {
			this.viewStateListener = new ArrayList<ViewStateListener>();
		}
		this.viewStateListener.add(listener);
		if(this.node != null) {
			this.attachListenerToNodes(listener);
		}
	}

	/**
	 * Returns the <tt>ViewStateManager</tt> instance of this view, that listens to all change events.
	 * 
	 * @return the <tt>ViewStateManager</tt> instance of this view
	 */
	public ViewStateManager getViewStateManager() {
		return viewStateManager;
	}

	/**
	 * Sets the <tt>ViewStateManager</tt> instance of this view, that listens to all change events.
	 * 
	 * @param viewStateManager the <tt>ViewStateManager</tt> instance to set
	 */
	public void setViewStateManager(ViewStateManager viewStateManager) {
		this.viewStateManager = viewStateManager;
	}

	/**
	 * Attaches the <tt>ViewStateListener</tt> to the nodes in the scene graph.
	 * @param listener the <tt>ViewStateListener</tt> that shall be registered with this view
	 */
	protected void attachListenerToNodes(ViewStateListener listener) {
		// apply listener to every supported note, using the node
		// handler and adapter
		NodeWrapper.traverse(getNode(), new ViewStateListenerNodeHandler(getNode(), listener));
	}
	
	public abstract void show();

}
