package org.bitbucket.afx.view.binding;

import java.beans.PropertyDescriptor;
import java.util.Map;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeansException;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.PropertyValues;
import org.springframework.core.convert.TypeDescriptor;

/**
 * Extension to <tt>BeanPathAdapter</tt> that provides
 * a wrapping facilities for the specified bean, in order
 * to access the bean's properties.
 * 
 * @author MartinKoster
 * 
 */
@SuppressWarnings("rawtypes")
public class BeanWrapperAdapter extends BeanPathAdapter {

	private BeanWrapper beanWrapper;

	/**
	 * Constructor that accepts the <tt>bean</tt> to wrap.
	 * 
	 * @param bean
	 *            the bean the {@link BeanWrapperAdapter} is for
	 */
	@SuppressWarnings("unchecked")
	public BeanWrapperAdapter(final Object bean) {
		super(bean);
		this.beanWrapper = PropertyAccessorFactory.forBeanPropertyAccess(bean);
	}

	/**
	 * Checks, whether the property found under <tt>propertyName</tt> is readable.
	 * 
	 * @param propertyName the name of the property
	 * @return <tt>true</tt>, if the given property is readable, <tt>false</tt> otherwise.
	 */
	public boolean isReadableProperty(String propertyName) {
		return beanWrapper.isReadableProperty(propertyName);
	}

	/**
	 * Returns the wrapped instance.
	 * @return the wrapped instance
	 */
	public Object getWrappedInstance() {
		return beanWrapper.getWrappedInstance();
	}

	/**
	 * Checks, whether the property found under <tt>propertyName</tt> is writable.
	 * 
	 * @param propertyName the name of the property
	 * @return <tt>true</tt>, if the given property is writable, <tt>false</tt> otherwise.
	 */
	public boolean isWritableProperty(String propertyName) {
		return beanWrapper.isWritableProperty(propertyName);
	}

	/**
	 * Returns the class type of the wrapped instance.
	 * @return the class type of the wrapped instance
	 */
	public Class<?> getWrappedClass() {
		return beanWrapper.getWrappedClass();
	}

	/**
	 * Returns the <tt>PropertyDescriptor</tt>s of the wrapped instance.
	 * @return the <tt>PropertyDescriptor</tt>s of the wrapped instance
	 */
	public PropertyDescriptor[] getPropertyDescriptors() {
		return beanWrapper.getPropertyDescriptors();
	}

	/**
	 * Returns the property type found under the property defined by <tt>propertyName</tt>.
	 * @return the property type 
	 */
	public Class<?> getPropertyType(String propertyName) throws BeansException {
		return beanWrapper.getPropertyType(propertyName);
	}

	/**
	 * Returns the <tt>PropertyDescriptor</tt>s found under the property defined by <tt>propertyName</tt>.
	 * @return the <tt>PropertyDescriptor</tt>s found under the property defined by <tt>propertyName</tt>
	 */
	public PropertyDescriptor getPropertyDescriptor(String propertyName) throws InvalidPropertyException {
		return beanWrapper.getPropertyDescriptor(propertyName);
	}

	/**
	 * Returns the <tt>TypeDescriptor</tt>s found under the property defined by <tt>propertyName</tt>.
	 * @return the <tt>TypeDescriptor</tt>s found under the property defined by <tt>propertyName</tt>
	 */
	public TypeDescriptor getPropertyTypeDescriptor(String propertyName) throws BeansException {
		return beanWrapper.getPropertyTypeDescriptor(propertyName);
	}

	/**
	 * Returns the property value from the property defined by <tt>propertyName</tt> by accessing the corresponding getter.
	 * @param propertyName the name of the property to access
	 * @return the value retrieved by invoking the corresponding getter
	 * @throws BeansException
	 */
	public Object getPropertyValue(String propertyName) throws BeansException {
		return beanWrapper.getPropertyValue(propertyName);
	}

	/**
	 * Sets a value to the property defined by <tt>propertyName</tt> by calling the corresponding setter
	 * @param propertyName the name of the property to set
	 * @param value the value to set
	 * @throws BeansException
	 */
	public void setPropertyValue(String propertyName, Object value) throws BeansException {
		beanWrapper.setPropertyValue(propertyName, value);
	}

	/**
	 * Sets a value of type <tt>PropertyValue</tt>.
	 * 
	 * @param pv the value to set
	 * @throws BeansException
	 */
	public void setPropertyValue(PropertyValue pv) throws BeansException {
		beanWrapper.setPropertyValue(pv);
	}

	/**
	 * Sets a map of values to the wrapped instance.
	 *  
	 * @param map the map of values for a bulk update 
	 * @throws BeansException
	 */
	public void setPropertyValues(Map<?, ?> map) throws BeansException {
		beanWrapper.setPropertyValues(map);
	}

	/**
	 * Sets values of type <tt>PropertyValues</tt>.
	 * 
	 * @param pvs the values to set
	 * @throws BeansException
	 */
	public void setPropertyValues(PropertyValues pvs) throws BeansException {
		beanWrapper.setPropertyValues(pvs);
	}

	/**
	 * Sets values of type <tt>PropertyValues</tt>.
	 * 
	 * @param pvs the values to set
	 * @param ignoreUnknown flag determining whether unknown properties shall be ignored or not
	 * @throws BeansException
	 */
	public void setPropertyValues(PropertyValues pvs, boolean ignoreUnknown) throws BeansException {
		beanWrapper.setPropertyValues(pvs, ignoreUnknown);
	}

	/**
	 * Sets values of type <tt>PropertyValues</tt>.
	 * 
	 * @param pvs the values to set
	 * @param ignoreUnknown flag determining whether unknown properties shall be ignored or not
	 * @param ignoreInvalid flag determining whether inaccessible properties shall be ignored or not
	 * @throws BeansException
	 */
	public void setPropertyValues(PropertyValues pvs, boolean ignoreUnknown, boolean ignoreInvalid)
			throws BeansException {
		beanWrapper.setPropertyValues(pvs, ignoreUnknown, ignoreInvalid);
	}
	
	
}