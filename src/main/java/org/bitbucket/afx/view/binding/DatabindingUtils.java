package org.bitbucket.afx.view.binding;

import java.util.Map;

import org.bitbucket.afx.view.NodeWrapper;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.scene.Node;

/**
 * Utility class for data binding.
 * 
 * @author MartinKoster
 *
 */
public class DatabindingUtils {

	/**
	 * Checks, whether data binding is supported for the given <tt>node</tt>.
	 * 
	 * @param node
	 *            the node to be checked
	 * @return <tt>true</tt>, in case the node supports a primary value (see
	 *         also {@link org.bitbucket.afx.view.NodeWrapper.getPrimaryValue}),
	 *         <tt>false</tt> otherwise.
	 */
	public static boolean supportsPrimaryValue(Node node) {
		return NodeWrapper.supportsPrimaryValue(node.getClass());
	}

	/**
	 * Extracts all values from nodes that have an "id" and and that support a
	 * primary value.
	 * 
	 * @param node the root node of JavaFX scene graph
	 * @return a map, where the key is the node ID and the value the retrieved value of that node
	 */
	public static Map<String, Object> extractValues(Node node) {
		ValueExtractingNodeHandler handler = new ValueExtractingNodeHandler();
		NodeWrapper.traverse(node, handler);
		return handler.getAllValues();
	}

	/**
	 * Binds the <tt>progressProperty</tt> of <tt>javafx.concurrent.Task</tt> to
	 * the given <tt>progressBarNode</tt>.
	 * 
	 * @param task the task that controls the progress indicator
	 * @param progressBarNode the progress bar instance
	 */
	public static void bindTaskToProgressProperty(Task<?> task,
			Node progressBarNode) {
		NodeWrapper wrapper = new NodeWrapper(progressBarNode);
		DoubleProperty property = (DoubleProperty) wrapper
				.getProperty(NodeWrapper.PROP_PROGRESSPROPERTY);
		property.unbind();
		property.set(0.0f);
		property.bind(task.progressProperty());
	}

	/**
	 * Unbinds the given <tt>progressBarNode</tt>.
	 * 
	 * @param progressBarNode the progress bar instance
	 */
	public static void unbindProgressProperty(Node progressBarNode) {
		NodeWrapper wrapper = new NodeWrapper(progressBarNode);
		DoubleProperty property = (DoubleProperty) wrapper
				.getProperty(NodeWrapper.PROP_PROGRESSPROPERTY);
		property.unbind();
		property.set(0.0f);
	}

	/**
	 * Binds the <tt>messageProperty</tt> of <tt>javafx.concurrent.Task</tt> to
	 * the given <tt>messageNode</tt>.
	 * 
	 * @param task the task that emits messages
	 * @param messageNode the node that will receive the emitted messages
	 */
	public static void bindTaskToMessageProperty(Task<?> task, Node messageNode) {
		NodeWrapper wrapper = new NodeWrapper(messageNode);
		StringProperty property = (StringProperty) wrapper
				.getProperty(NodeWrapper.PROP_TEXTPROPERTY);
		property.unbind();
		property.set("");
		property.bind(task.messageProperty());
	}

	/**
	 * Unbinds the given <tt>messageNode</tt>.
	 * 
	 * @param messageNode the node
	 */
	public static void unbindMessageProperty(Node messageNode) {
		NodeWrapper wrapper = new NodeWrapper(messageNode);
		StringProperty property = (StringProperty) wrapper
				.getProperty(NodeWrapper.PROP_TEXTPROPERTY);
		property.unbind();
		property.set("");
	}

}
