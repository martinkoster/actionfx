package org.bitbucket.afx.view.binding;

import java.util.HashMap;
import java.util.Map;

import org.bitbucket.afx.view.NodeHandler;
import org.bitbucket.afx.view.NodeWrapper;

import javafx.scene.Node;

/**
 * Node-handler that extracts all values from fields that support data binding.
 * <p>
 * See also: {@link org.bitbucket.afx.view.NodeWrapper.getPrimaryValue}
 * 
 * @author Martin
 *
 */
public class ValueExtractingNodeHandler implements NodeHandler {

	private Map<String, Object> allValues = new HashMap<String, Object>();
	
	public ValueExtractingNodeHandler() {
	}
	
	@Override
	public void process(Node node) {
		if(NodeWrapper.supportsPrimaryValue(node.getClass())) {
			NodeWrapper wrapper = new NodeWrapper(node);
			this.allValues.put(node.getId(), wrapper.getPrimaryValue());
		}
	}

	/**
	 * Returns a map, where the key is the node ID and the value the retrieved value of that node.
	 * @return a map, where the key is the node ID and the value the retrieved value of that node
	 */
	public Map<String, Object> getAllValues() {
		return allValues;
	}

}
