package org.bitbucket.afx.view.binding;

import java.util.HashMap;
import java.util.Map;

import org.bitbucket.afx.view.NodeHandler;
import org.bitbucket.afx.view.NodeWrapper;

import javafx.scene.Node;

/**
 * Implementation of <tt>NodeHandler</tt> that collects nodes
 * from the scene graph that support data binding in the means
 * that there is a primary value property.
 * <p>
 * See also: {@link org.bitbucket.afx.view.NodeWrapper.getPrimaryValue}
 * @author MartinKoster
 *
 */
public class BindingCapableNodeHandler implements NodeHandler {

	private Map<String, Node> nodeMap = new HashMap<String, Node>();
	
	public BindingCapableNodeHandler() {
	}
	
	@Override
	public void process(Node node) {
		String id = node.getId();
		if(id != null &&  NodeWrapper.supportsPrimaryValue(node.getClass())) {
			this.nodeMap.put(id, node);
		}
	}

	/**
	 * Returns a map, where the key is the node ID and the value the node that is supporting a data binding. 
	 * @return a map, where the key is the node ID and the value the node that is supporting a data binding
	 */
	public Map<String, Node> getNodeMap() {
		return nodeMap;
	}
}
