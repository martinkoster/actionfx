package org.bitbucket.afx;

import javafx.stage.Stage;

/**
 * Interface for classes that hold the primary <tt>javafx.stage.Stage</code> that is constructed
 * by JavaFX on startup.
 * 
 * @author MartinKoster
 *
 */
public interface AFXPrimaryStageHolder {

	/**
	 * Get the primary stage.
	 * 
	 * @return the primary stage constructed by JavaFX on startup
	 */
	public Stage getPrimaryStage();
	
	/**
	 * Set the primary stage.
	 * 
	 * @param primaryStage the primary stage to set
	 */
	public void setPrimaryStage(Stage primaryStage);
}
