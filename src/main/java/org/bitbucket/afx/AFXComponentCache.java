package org.bitbucket.afx;


/**
 * Interface for caching ActionFX-related instances. This interface abstracts away
 * the underlying container that is managing the JavaBean-instances.
 * 
 * @author MartinKoster
 *
 */
public interface AFXComponentCache {

	/**
	 * Stores a components in the underlying container.
	 * 
	 * @param id the ID of the component
	 * @param component the component to store
	 */
	public void storeComponent(String id, Object component);
	
	/**
	 * Retrieves a component from the underlying container.
	 * 
	 * @param id the ID of the component that shall be looked up
	 * @return the component found for the given ID
	 */
	public Object lookupComponent(String id);
}
