package org.bitbucket.afx.spring.aop;

import java.lang.reflect.Method;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Node;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.SuppressAjWarnings;
import org.aspectj.lang.reflect.MethodSignature;
import org.bitbucket.afx.AFXComponentCache;
import org.bitbucket.afx.VoidMethodCallback;
import org.bitbucket.afx.annotation.FxAction;
import org.bitbucket.afx.annotation.FxView;
import org.bitbucket.afx.utils.AFXExpressionUtils;
import org.bitbucket.afx.utils.AFXUtils;
import org.bitbucket.afx.utils.ReflectionUtils;
import org.bitbucket.afx.view.AbstractErrorHandlingView;
import org.bitbucket.afx.view.AbstractView;
import org.bitbucket.afx.view.View;
import org.bitbucket.afx.view.binding.DatabindingUtils;
import org.bitbucket.afx.view.fxml.AbstractFxmlDatabindingView;
import org.bitbucket.afx.view.validation.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.annotation.AnnotationUtils;

/**
 * Aspect intercepting all <tt>@FxAction</tt> annotated methods.
 * 
 * @author MartinKoster
 *
 */
@Aspect
@Configurable
public class AFXControllerInterceptor {

	private static final Log LOG = LogFactory.getLog(AFXControllerInterceptor.class);

	@Autowired
	private AFXComponentCache afxComponentCache;

	/**
	 * Point-cut targeting at all public methods.
	 */
	@Pointcut("execution(public * *(..))")
	public void anyPublicMethod() {
	}

	/**
	 * Point-cut targetting at all class carrying a @FxController annotation.
	 */
	@Pointcut("within(@org.bitbucket.afx.annotation.FxController *)")
	public void anyClassWithJFXController() {
	}

	/**
	 * Around method woven around all public controller methods.
	 * 
	 * @param pjp
	 *            the <tt>ProceedingJoinPoint</tt> that holds access to the
	 *            intercepted method
	 * @return the value returned by the intercepted method
	 * @throws Throwable
	 */
	@SuppressAjWarnings("adviceDidNotMatch")
	@Around("anyClassWithJFXController() && anyPublicMethod()")
	public Object handlePublicControllerMethod(final ProceedingJoinPoint pjp) throws Throwable {

		final Object controller = pjp.getTarget();
		final Method method = ((MethodSignature) pjp.getStaticPart().getSignature()).getMethod();

		FxAction jfxAction = AnnotationUtils.findAnnotation(method, FxAction.class);
		FxView jfxView = AnnotationUtils.findAnnotation(controller.getClass(), FxView.class);

		View view = null;
		if (jfxView != null) {
			view = this.lookupView(jfxView.id());
		}
		if (jfxAction != null) {
			return this.handleJFXAction(pjp, controller, method, jfxAction, view);
		} else {
			return this.handleNonJFXAction(pjp, controller, method, view);
		}
	}

	/**
	 * Handler method that handles controller methods annotated by
	 * <tt>@FxAction</tt>.
	 * 
	 * @param pjp
	 *            the <tt>ProceedingJoinPoint</tt> that holds access to the
	 *            intercepted method
	 * @param controller
	 *            the controller that possesses the intercepted method
	 * @param method
	 *            the intercepted method
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation that is applied on the
	 *            method
	 * @param view
	 *            the view instance that is associated with the controller
	 * @return the value returned by the intercepted method
	 * @throws Throwable
	 */
	public Object handleJFXAction(final ProceedingJoinPoint pjp, Object controller, Method method, FxAction fxAction,
			View view) throws Throwable {
		Object retVal = null;
		if (fxAction.asynchronous()) {
			this.executeJoinPointAsynchronously(pjp, controller, method, fxAction, view);
		} else {
			this.executeJoinPointSynchronously(pjp, controller, method, fxAction, view);
		}
		if (!ReflectionUtils.isCalledFromMethod(this.getClass().getName(), "updateModel")) {
			this.updateModel(view);
		}
		return retVal;
	}

	/**
	 * Handler method that handles all controller methods that are <b>not</b>
	 * annotated by <tt>@FxAction</tt>.
	 * <p>
	 * This methods takes care that changes in the model are reflected also in
	 * the UI.
	 * 
	 * @param pjp
	 *            the <tt>ProceedingJoinPoint</tt> that holds access to the
	 *            intercepted method
	 * @param controller
	 *            the controller that possesses the intercepted method
	 * @param method
	 *            the intercepted method
	 * @param view
	 *            the view instance that is associated with the controller
	 * @return the value returned by the intercepted method
	 * @throws Throwable
	 */
	public Object handleNonJFXAction(final ProceedingJoinPoint pjp, Object controller, Method method, View view)
			throws Throwable {
		Object retVal = null;
		retVal = pjp.proceed();
		if (!ReflectionUtils.isCalledFromMethod(this.getClass().getName(), "updateModel")) {
			this.updateModel(view);
		}
		return retVal;
	}

	/**
	 * Execute the given <code>ProceedingJointPoint</code> in an asynchronous
	 * fashion.
	 * 
	 * @param pjp
	 *            the <tt>ProceedingJoinPoint</tt> that holds access to the
	 *            intercepted method
	 * @param controller
	 *            the controller that possesses the intercepted method
	 * @param method
	 *            the intercepted method
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation that is applied on the
	 *            method
	 * @param view
	 *            the view instance that is associated with the controller
	 * @throws Throwable
	 */
	public void executeJoinPointAsynchronously(final ProceedingJoinPoint pjp, final Object controller,
			final Method method, final FxAction fxAction, final View view) throws Throwable {

		final Node progressNode = this.determineProgressNode(fxAction);

		this.invokeInNewThread(() -> {

			// handle onBefore event
				this.handleOnBefore(fxAction, controller);

				// handle validation (throws a ValidationException, if
				// validation failed)
				this.handleValidation(fxAction, view);

				try {

					// execute actual surrounded method
					LOG.debug("Invoking method '" + controller.getClass().getName() + "." + method.getName()
							+ "' asynchronosly in new thread.");

					pjp.proceed();

					// refresh data binding ()
					// this.refreshBindings(view);

				} catch (Throwable e) {
					if (e instanceof Exception) {
						throw (Exception) e;
					} else {
						throw new Exception(e);
					}
				}

				// handle onAfter event
				this.handleOnAfter(fxAction, controller);

			}, fxAction, progressNode);

	}

	/**
	 * Execute the given <code>ProceedingJointPoint</code> in a synchronous
	 * fashion.
	 * 
	 * @param pjp
	 *            the <tt>ProceedingJoinPoint</tt> that holds access to the
	 *            intercepted method
	 * @param controller
	 *            the controller that possesses the intercepted method
	 * @param method
	 *            the intercepted method
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation that is applied on the
	 *            method
	 * @param view
	 *            the view instance that is associated with the controller
	 * @throws Exception
	 */
	public void executeJoinPointSynchronously(final ProceedingJoinPoint pjp, final Object controller,
			final Method method, final FxAction fxAction, final View view) throws Throwable {
		try {

			// handle onBefore event
			this.handleOnBefore(fxAction, controller);

			// handle validation (throws a ValidationException, if validation
			// failed)
			this.handleValidation(fxAction, view);

			// execute actual surrounded method
			LOG.debug("Invoking method '" + controller.getClass().getName() + "." + method.getName()
					+ "' synchronously in current thread.");

			pjp.proceed();

			// refresh data binding ()
			// this.refreshBindings(view);

			// handle onAfter event
			this.handleOnAfter(fxAction, controller);

			// handleSuccessView
			this.handleOnSuccessView(fxAction);

		} catch (Throwable throwable) {
			LOG.debug("", throwable);
			if (throwable instanceof Exception) {

				// handleFailureView
				this.handleOnFailureView(fxAction, (Exception) throwable);
			} else {
				throw throwable;
			}
		}
	}

	/**
	 * Handles validation by evaluating the corresponding setting in annotation
	 * <tt>FxAction</tt>.
	 * 
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation
	 * @param view
	 *            the view associated with the controller that got intercepted
	 */
	protected void handleValidation(FxAction fxAction, View view) throws ValidationException {
		if (fxAction.requiresSuccessfulValidation()) {
			if (view instanceof AbstractView) {
				AbstractView abstractView = (AbstractView) view;
				abstractView.getValidationHandler().throwValidationExceptionIfHasErrors();
			}
		}
	}

	/**
	 * Executes the code supplied in the <code>VoidMethodCallback</code> in a
	 * separate thread.
	 * 
	 * @param callback
	 *            the callback to be executed in a new thread
	 */
	public void invokeInNewThread(final VoidMethodCallback callback, final FxAction jfxAction, final Node progressNode) {

		Task<Void> task = new Task<Void>() {
			@Override
			protected Void call() throws Exception {
				callback.executeMethod();
				return null;
			}

			@Override
			protected void succeeded() {
				try {
					if (progressNode != null) {
						DatabindingUtils.unbindProgressProperty(progressNode);
					}
					invokeMethod(() -> handleOnSuccessView(jfxAction), true);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					if (e instanceof RuntimeException) {
						throw (RuntimeException) e;
					} else {
						throw new RuntimeException("", e);
					}
				}
			}

			@Override
			protected void cancelled() {
				if (progressNode != null) {
					DatabindingUtils.unbindProgressProperty(progressNode);
				}
			}

			@Override
			protected void failed() {
				try {
					if (progressNode != null) {
						DatabindingUtils.unbindProgressProperty(progressNode);
					}
					invokeMethod(() -> handleOnFailureView(jfxAction, this.getException()), true);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					if (e instanceof RuntimeException) {
						throw (RuntimeException) e;
					} else {
						throw new RuntimeException("", e);
					}
				}
			}
		};

		if (progressNode != null) {
			DatabindingUtils.bindTaskToProgressProperty(task, progressNode);
		}

		LOG.debug("Starting async task.");
		Thread thread = new Thread(task);
		thread.setDaemon(true);
		thread.start();
	}

	/**
	 * Executes the given <tt>VoidMethodCallback</tt> either in the current
	 * thread of execution or in the JavaFX thread using
	 * <tt>Platform.runLater()</tt>.
	 * 
	 * @param callback
	 *            the callback to be executed
	 * @param useRunLater
	 *            flag, whether the the callback shall be executed using
	 *            <tt>Platform.runLater()</tt>
	 */
	public static void invokeMethod(final VoidMethodCallback callback, boolean useRunLater) throws Exception {
		if (useRunLater) {
			Platform.runLater(AFXUtils.getWrapper(() -> {
				try {
					callback.executeMethod();
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					if (e instanceof RuntimeException) {
						throw (RuntimeException) e;
					} else {
						throw new RuntimeException(e);
					}
				}
			}));
		} else {
			callback.executeMethod();
		}
	}

	/**
	 * Handle onBefore event, if supplied in the <code>FxAction</code>
	 * annotation.
	 * 
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation
	 * @param controller
	 *            the controller that got intercepted
	 */
	protected void handleOnBefore(FxAction fxAction, Object controller) {
		if (StringUtils.trimToNull(fxAction.onBefore()) != null) {
			Object target = this.determineComponent(fxAction.onBefore(), controller);
			Method method = ReflectionUtils
					.findMethod(target.getClass(), this.determineMethodName(fxAction.onBefore()));
			LOG.debug("onBefore: Invoking method '" + method.getName() + "' on class '" + target.getClass().getName()
					+ "'.");
			ReflectionUtils.invokeMethod(method, target);
		}
	}

	/**
	 * Handle onAfter event, if supplied in the <code>FxAction</code>
	 * annotation.
	 * 
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation
	 * @param controller
	 *            the controller that got intercepted
	 */
	protected void handleOnAfter(FxAction fxAction, Object controller) {
		if (StringUtils.trimToNull(fxAction.onAfter()) != null) {
			Object target = this.determineComponent(fxAction.onAfter(), controller);
			Method method = ReflectionUtils.findMethod(target.getClass(), this.determineMethodName(fxAction.onAfter()));
			LOG.debug("onAfter: Invoking method '" + method.getName() + "' on class '" + target.getClass().getName()
					+ "'.");
			ReflectionUtils.invokeMethod(method, target);
		}
	}

	/**
	 * Determines the node responsible for tracking the progress.
	 * 
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation
	 * @return the resolved node
	 */
	protected Node determineProgressNode(FxAction fxAction) {
		Node progressNode = null;
		final View view = (View) this.determineComponent(fxAction.progressIndicatorId(), null);
		final String nodeId = this.determineNodeId(fxAction.progressIndicatorId());
		if (view != null) {
			progressNode = AFXUtils.findNodeById(view.getNode(), nodeId);
		}
		return progressNode;
	}

	/**
	 * Determines the correct component defined by the supplied <tt>path</tt>.
	 * If <tt>path</tt> is a nested path with dot-notation, the first name in
	 * the path is supposed to be bean name that is retrieved from the component
	 * cache.
	 * 
	 * @param path
	 *            the path to be resolved to a component
	 * @param defaultComponent
	 *            the default component to be returned, in case the path can not
	 *            be resolved
	 * @return the retrieved component
	 */
	protected Object determineComponent(String path, Object defaultComponent) {
		Object retVal = defaultComponent;
		String baseName = AFXExpressionUtils.getComponentName(path);
		if (baseName != null) {
			retVal = this.afxComponentCache.lookupComponent(baseName);
		}
		return retVal;
	}

	/**
	 * Determines the correct method name defined by the supplied <tt>path</tt>.
	 * If <tt>path</tt> is a nested path with dot-notation, the second name in
	 * the path is supposed to be the method name.
	 * 
	 * @param path
	 *            the path to the method
	 * @return the name of the method, extracted from the given path
	 */
	protected String determineMethodName(String path) {
		String methodName = AFXExpressionUtils.getMethodName(path);
		return methodName;
	}

	/**
	 * Determines the correct node ID defined by the supplied <tt>path</tt>. If
	 * <tt>path</tt> is a nested path with dot-notation, the second name in the
	 * path is supposed to be the node ID.
	 * 
	 * @param path
	 *            the path to the node
	 * @return the node ID extracted from the path
	 */
	protected String determineNodeId(String path) {
		String nodeId = AFXExpressionUtils.getNodeId(path);
		return nodeId;
	}

	/**
	 * If the processing of the controller call was successful, this method is
	 * responsible for displaying the success view.
	 * 
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation
	 */
	protected void handleOnSuccessView(FxAction fxAction) {
		if (StringUtils.trimToNull(fxAction.onSuccessView()) != null) {
			View view = this.lookupView(fxAction.onSuccessView());
			LOG.debug("Displaying onSuccessView with ID='" + view.getId() + "'");
			view.show();
		}
	}

	/**
	 * In case an error in form of an <code>Exception</code> occurs, this method
	 * is responsible for displaying the error in the error view.
	 * 
	 * @param fxAction
	 *            the <tt>@FxAction</tt> annotation
	 * @param exception
	 *            the exception that occured during method invocation
	 */
	protected void handleOnFailureView(FxAction fxAction, Throwable exception) {
		if (StringUtils.trimToNull(fxAction.onFailureView()) != null) {
			View view = this.lookupView(fxAction.onFailureView());
			LOG.debug("Displaying onFailureView with ID='" + view.getId() + "' for exception '"
					+ exception.getClass().getName() + "'.");
			if (view instanceof AbstractErrorHandlingView) {
				((AbstractErrorHandlingView) view).setThrowable(exception);
			}
			view.show();
		}
	}

	/**
	 * Retrieves a view from the <code>AFXComponentCache</code>.
	 * 
	 * @param id
	 *            the ID of the component to look up
	 * @return the <tt>View</tt> instance that has been found for the supplied
	 *         ID
	 */
	protected View lookupView(String id) {
		Object candidate = this.afxComponentCache.lookupComponent(id);
		if (candidate == null) {
			throw new IllegalStateException("No component with ID='" + id + "' has been found!");
		}
		if (!(candidate instanceof View)) {
			throw new IllegalStateException("component with ID='" + id + "' is not a view instance, type is '"
					+ candidate.getClass().getName() + "'!");
		}
		return (View) candidate;
	}

	/**
	 * Refresh bindings of the view. The method checks, if it has been already
	 * part of the execution stack in order to avoid recursive calls, because
	 * model updates call controller methods which leads again to intercepting
	 * methods, etc.
	 * 
	 * @param view
	 *            the view that holds the bindings
	 */
	protected void updateModel(View view) {
		if (view instanceof AbstractFxmlDatabindingView) {
			AbstractFxmlDatabindingView databindingView = (AbstractFxmlDatabindingView) view;
			databindingView.refreshBindings();
		}
	}

	/**
	 * Returns the set <tt>AFXComponentCache</tt> instance.
	 * 
	 * @return the set <tt>AFXComponentCache</tt> instance
	 */
	public AFXComponentCache getAFXComponentCache() {
		return afxComponentCache;
	}

	/**
	 * Sets <tt>AFXComponentCache</tt> instance.
	 * 
	 * @param afxComponentCache
	 *            the <tt>AFXComponentCache</tt> instance
	 */
	public void setAFXComponentCache(AFXComponentCache afxComponentCache) {
		this.afxComponentCache = afxComponentCache;
	}

}
