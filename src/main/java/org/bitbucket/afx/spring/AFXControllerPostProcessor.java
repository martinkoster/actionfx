package org.bitbucket.afx.spring;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.annotation.FxController;
import org.bitbucket.afx.annotation.FxInitBehavior;
import org.bitbucket.afx.annotation.FxView;
import org.bitbucket.afx.utils.ReflectionUtils;
import org.bitbucket.afx.view.AbstractView;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;

/**
 * <code>BeanPostProcessor</code> responsible for initializing JavaFX
 * views, derived from <code>View</code>. Only annotations on class that are
 * annotated with the <code>FxController</code> annotation are considered.
 * 
 * @author MartinKoster
 *
 */
public class AFXControllerPostProcessor implements BeanPostProcessor,
		InitializingBean {

	private static final Log LOG = LogFactory.getLog(AFXControllerPostProcessor.class);
	
	private AFXViewInitializer afxViewInitializer;

	/**
	 * Evaluates, whether the initialized Spring bean carries a <code>FxController</code>
	 * annotation. If so, all views defined by the <code>FxView</code>
	 * annotation are post-processed.
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String name)
			throws BeansException {

		// extract the annotation
		FxController jfxController = AnnotationUtils.findAnnotation(
				bean.getClass(), FxController.class);

		if (jfxController != null) {

			FxView jfxView = AnnotationUtils.findAnnotation(bean.getClass(), FxView.class);

			if (jfxView != null) {

				// lookup the correct view
				AbstractView view = (AbstractView) this.afxViewInitializer.lookupComponent(jfxView.id());

				// initialize internal fields (e.g. load the FXML from file)
				this.afxViewInitializer.postProcessView(view, bean);
				
				// init view behavior
				this.initBehavior(bean, view);
			
			}
		}

		return bean;
	}
	
	/**
	 * Initialize the view's behavior, in case the controller has a <tt>FxInitBehavior</tt> annotation.
	 * 
	 * @param controller the controller that holds the behavior method
	 * @param view the view that shall receive the behavior definition
	 */
	protected void initBehavior(Object controller, AbstractView view) {
		Method initBehaviorMethod = ReflectionUtils.findAnnotatedMethod(controller.getClass(), FxInitBehavior.class);
		if(initBehaviorMethod == null) {
			LOG.debug("FxController '" + controller.getClass().getName() + "' has no FxInitBehavior annotation. Skip view behavior initialization.");
			return;
		}		 
		
		LOG.debug("FxController '" + controller.getClass().getName() + "' has FxInitBehavior annotation. Invoking method '" + initBehaviorMethod.getName() + "'.");
		
		Class<?>[] parameterTypes = initBehaviorMethod.getParameterTypes();
		Object[] args = ReflectionUtils.matchInstancesToParameterTypes(parameterTypes, controller, view, view.getViewStateManager(), view.getComponentCache(), view.getValidationHandler());
		
		ReflectionUtils.invokeMethod(initBehaviorMethod, controller, args);
		
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String name)
			throws BeansException {
		return bean;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (this.afxViewInitializer == null) {
			throw new FatalBeanException(
					"Property 'afxViewInitializer' must be initialized!");
		}
	}

	/**
	 * Returns the set <tt>AFXViewInitializer</tt> instance.
	 * 
	 * @return the set <tt>AFXViewInitializer</tt> instance
	 */
	public AFXViewInitializer getAFXViewInitializer() {
		return afxViewInitializer;
	}

	/**
	 *  Set <tt>AFXViewInitializer</tt> instance.
	 *  
	 * @param afxViewInitializer the  <tt>AFXViewInitializer</tt> instance to set
	 */
	public void setAFXViewInitializer(AFXViewInitializer afxViewInitializer) {
		this.afxViewInitializer = afxViewInitializer;
	}
}
