package org.bitbucket.afx.spring;

import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.annotation.FxController;
import org.bitbucket.afx.annotation.FxView;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.ClassUtils;

/**
 * <code>BeanFactoryPostProcessor</code> responsible for initializing all annotated JavaFX components, like  controllers that are annotated 
 * with the <code>FxController</code> annotation. 
 * 
 * @author MartinKoster
 *
 */
public class AFXComponentsBeanFactoryPostProcessor implements
		BeanFactoryPostProcessor, ApplicationContextAware {

	private static final Log LOG = LogFactory.getLog(AFXComponentsBeanFactoryPostProcessor.class);
	
	public static final String BEANNAME_AFXVIEWINITIALIZER = "afxViewInitializer";

	public static final String BEANNAME_AFXCONTROLLERPOSTPROCESSOR = "afxControllerPostProcessor";
	
	private String basePackage;
	
	private ApplicationContext applicationContext;
	
	private MessageSource messageSource;
	
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

		// register AFXViewInitializer and AFXControllerPostProcessor
		this.registerAFXProcessors(beanFactory);
		
		
		// scan for FxController annotations...
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
		scanner.addIncludeFilter(new AnnotationTypeFilter(FxController.class));
		Set<BeanDefinition> definitions = scanner.findCandidateComponents(this.basePackage == null ? "" : this.basePackage );
		if(definitions != null) {
			
			for(BeanDefinition definition : definitions) {
				
				// inspect class given in BeanDefiniton
				FxController jfxController = this.extractJFXControllerAnnotation(definition);
				if(jfxController != null) {
					this.registerBeanDefinition(StringUtils.trimToNull(jfxController.id()) == null ? jfxController.value() : jfxController.id(), definition, beanFactory);
					
					// check for FxView annotation
					FxView jfxView = this.extractJFXViewAnnotation(definition);
					if(jfxView != null) {
						BeanDefinition jfxViewBeanDefinition = this.buildBeanDefinition(jfxView);
						this.registerBeanDefinition(jfxView.id(), jfxViewBeanDefinition, beanFactory);
					}
				}
			}
		}
	}
	
	/**
	 * Extracts the <code>FxController</code> annotation from the class given in the <code>BeanDefinition</code>.
	 * If the annotation is not present on that class, <tt>null</tt> will be returned.
	 * 
	 * @param definition the bean definition
	 * @return the <tt>@FxController</tt> annotation
	 */
	protected FxController extractJFXControllerAnnotation(BeanDefinition definition) {
		return AnnotationUtils.findAnnotation(this.resolveClassName(definition), FxController.class);
	}

	/**
	 * Extracts the <code>FxView</code> annotation from the class given in the <code>BeanDefinition</code>.
	 * If the annotation is not present on that class, <tt>null</tt> will be returned.
	 * 
	 * @param definition the bean definition
	 * @return the <tt>@FxView</tt> annotation
	 */
	protected FxView extractJFXViewAnnotation(BeanDefinition definition) {
		return AnnotationUtils.findAnnotation(this.resolveClassName(definition), FxView.class);
	}
	
	/**
	 * Resolves the class from a given bean definition.
	 * 
	 * @param definition the bean definition
	 * @return the class behind the given bean definition
	 */
	protected Class<?> resolveClassName(BeanDefinition definition) {
		return ClassUtils.resolveClassName(definition.getBeanClassName(), ClassUtils.getDefaultClassLoader());	
	}
	
	/**
	 * Creates a <code>BeanDefinition</code> based on <code>FxView</code> annotation.
	 * 
	 * @param jfxView the <tt>@FxView</tt> annotation
	 * @return the bean definition constructed based on the supplied annotation
	 */
	protected BeanDefinition buildBeanDefinition(FxView jfxView) {
		BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(AFXViewInitializer.resolveViewClass(jfxView));

		builder.addPropertyValue("id", jfxView.id());
		builder.addPropertyValue("title", jfxView.title());
		builder.addPropertyValue("sizeX", jfxView.sizeX());
		builder.addPropertyValue("sizeY", jfxView.sizeY());
		builder.addPropertyValue("modal", jfxView.modal());
		builder.addPropertyValue("maximized", jfxView.maximized());
//		builder.addPropertyValue("parentId", jfxView.parentId());
//		builder.addPropertyValue("position", jfxView.position());
		builder.addPropertyValue("fxml", jfxView.fxml());
		
		return builder.getBeanDefinition();
	}
	
	/**
	 * Registers the <code>AFXViewFactory</code> and <code>AFXControllerPostProcessor</code> with the Spring context.
	 * @param beanFactory the bean factory to use for registration
	 */
	protected void registerAFXProcessors(ConfigurableListableBeanFactory beanFactory) {
		AFXViewInitializer  afxViewInitializer = new AFXViewInitializer();
		afxViewInitializer.setApplicationContext(applicationContext);
		afxViewInitializer.setMessageSource(messageSource);
		afxViewInitializer.setLocale(Locale.getDefault());
		this.registerSingleton(BEANNAME_AFXVIEWINITIALIZER, afxViewInitializer, beanFactory);
		
		AFXControllerPostProcessor  jfxControllerPostProcessor = new AFXControllerPostProcessor();
		jfxControllerPostProcessor.setAFXViewInitializer(afxViewInitializer);
		try {
			jfxControllerPostProcessor.afterPropertiesSet();
		}
		catch(Exception e) {
			throw new FatalBeanException("", e);
		}
		this.registerSingleton(BEANNAME_AFXCONTROLLERPOSTPROCESSOR, jfxControllerPostProcessor, beanFactory);
	}

	/**
	 * Registers the <code>BeanDefinition</code> with the Spring context.
	 * 
	 * @param beanName the bean name
	 * @param definition the bean definition
	 * @param beanFactory the bean factory
	 */
	protected void registerBeanDefinition(String beanName, BeanDefinition definition, ConfigurableListableBeanFactory beanFactory) {
		BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) beanFactory;
		beanDefinitionRegistry.registerBeanDefinition(beanName, definition);
		LOG.info("Registered BeanDefinition of type '" + definition.getBeanClassName() + "' with bean name '" + beanName + "'.");
	}

	
	/**
	 * Registers the given bean under the provided name in the given bean factory.
	 * 
	 * @param beanName the bean name
	 * @param singletonObject the singleton instance to register
	 * @param beanFactory the bean factory
	 */
	protected void registerSingleton(String beanName, Object singletonObject, ConfigurableListableBeanFactory beanFactory) {
		beanFactory.registerSingleton(beanName, singletonObject);
		if(LOG.isInfoEnabled()) {
			LOG.info("Registered singleton instance of type '" + singletonObject.getClass().getName() + "' with bean name '"+ beanName + "'.");
		}
	}

	/**
	 * Gets the base package name to scan for components.
	 * 
	 * @return the base package name to scan for components
	 */
	public String getBasePackage() {
		return basePackage;
	}

	/**
	 * Sets the base package name to scan for components.
	 * 
	 * @param basePackage the base package name
	 */
	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

	/**
	 * Gets the message source.
	 * 
	 * @return the message source
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * Sets the message source.
	 * 
	 * @param messageSource the message source to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}	
}
