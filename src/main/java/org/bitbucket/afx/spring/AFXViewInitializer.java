package org.bitbucket.afx.spring;

import java.util.Locale;

import javafx.scene.Node;
import javafx.stage.Stage;

import org.bitbucket.afx.AFXComponentCache;
import org.bitbucket.afx.AFXMessageSource;
import org.bitbucket.afx.AFXPrimaryStageHolder;
import org.bitbucket.afx.annotation.FxView;
import org.bitbucket.afx.utils.AFXUtils;
import org.bitbucket.afx.view.AbstractFxmlView;
import org.bitbucket.afx.view.AbstractView;
import org.bitbucket.afx.view.View;
import org.bitbucket.afx.view.fxml.AbstractFxmlDatabindingView;
import org.bitbucket.afx.view.fxml.FxmlWindowView;
import org.bitbucket.afx.view.validation.impl.ValidationHandlerImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;

/**
 * Initializer-class for post-processing JavaFX views.
 * <p>
 * <b>Note:</b> It is required that the attribute <tt>primaryStage</tt> is set in <tt>org.bitbucket.afx.AFXApplication.start(Stage stage)</tt> 
 * method, so that the view has access to the primary <tt>Stage</tt> constructed by JavaFX.
 * 
 * @author MartinKoster
 *
 */
public class AFXViewInitializer implements ApplicationContextAware, AFXComponentCache, AFXPrimaryStageHolder, AFXMessageSource {

	private ApplicationContext context;
	
	private Stage primaryStage;

	private MessageSource messageSource;
	
	private Locale locale;
	
	/**
	 * Determines which view implementation class will be used for the given
	 * <code>FxView</code> annotation.
	 * 
	 * @param fxView the <tt>@FxView</tt> annotation	  
	 * @return the class implementing the functionality request by the supplied annotation
	 */
	public static Class<?> resolveViewClass(FxView fxView) {
		if(fxView == null) {
			return null;
		} else {
			return FxmlWindowView.class;
		} 
	}

	
	/**
	 * Performs a post-processing on the given view instance and initializes all internal fields. 
	 *  
	 * @param view the view that shall be post-processed
	 * @param controller the controller belonging to the given <tt>view</tt>
	 */
	public void postProcessView(AbstractView view, Object controller) {
		
		// initialize FXML related properties
		if(view instanceof AbstractFxmlView) {
			AbstractFxmlView fxmlView = (AbstractFxmlView) view;
			if(fxmlView.getFxml() != null) {
				Node fxmlNode = null;
				if(controller != null) {
					fxmlNode = (Node) AFXUtils.loadFxml(fxmlView.getFxml(), controller);
				} else {
					fxmlNode = (Node)  AFXUtils.loadFxml(fxmlView.getFxml());
				}
				view.setNode(fxmlNode);
			}
		}
		
		// initialize databinding related properties
		if(controller != null && (view instanceof AbstractFxmlDatabindingView)) {
			AbstractFxmlDatabindingView fxmlDatabindingView = (AbstractFxmlDatabindingView) view;
			fxmlDatabindingView.setModel(controller);
		}
		
		view.setPrimaryStageHolder(this);
		view.setComponentCache(this);
		view.setMessageSource(this);
		if(controller != null) {
			view.setValidationHandler(new ValidationHandlerImpl(controller, view.getNode(), this));
		}
	}
	
	public void storeComponent(String id, Object component) {
		DefaultListableBeanFactory factory = (DefaultListableBeanFactory) this.context.getAutowireCapableBeanFactory();
		factory.registerSingleton(id, component);  
	}
	
	public Object lookupComponent(String id) {
		Object bean = this.context.getBean(id);
		if(bean != null && bean instanceof View) {
			return (View) bean;
		}
		return null;
	}
	
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	@Override
	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		this.context = context;
	}

	/**
	 * Gets the <tt>MessageSource</tt> instance.
	 * @return the <tt>MessageSource</tt> instance
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}


	/**
	 * Sets the <tt>MessageSource</tt> instance
	 * @param messageSource the <tt>MessageSource</tt> instance
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}
	
	@Override
	public String getMessage(String code, Object[] args, String defaultMessage) {
		return this.messageSource.getMessage(code, args, defaultMessage, this.getLocale());
	}

	@Override
	public String getMessage(String code, Object[] args)
			throws NoSuchMessageException {
		return this.messageSource.getMessage(code, args, this.getLocale());
	}

	@Override
	public void setLocale(Locale locale) {
		this.locale = locale;
	}


	@Override
	public Locale getLocale() {
		return locale;
	}
	
}
