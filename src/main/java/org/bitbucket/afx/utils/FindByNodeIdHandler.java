package org.bitbucket.afx.utils;

import org.bitbucket.afx.view.NodeHandler;

import javafx.scene.Node;

/**
 * Implementation of the <tt>NodeHandler</tt> interface for 
 * finding a node by its ID.
 * 
 * @author MartinKoster
 *
 */
public class FindByNodeIdHandler implements NodeHandler {

	private String id;
	
	private boolean found = false;
	
	private Node result = null;
	
	/**
	 * Constructor accepting the node ID to search for
	 * @param id the node ID to search for
	 */
	public FindByNodeIdHandler(String id) {
		this.id = id;
	}

	@Override
	public void process(Node node) {
		if(node.getId() != null && node.getId().equals(this.id)) {
			this.result = node;
			this.found = true;
		}
	}

	@Override
	public boolean continueTraversing() {
		return !found;
	}
	
	/**
	 * Returns the result, if found. If the node could not be
	 * found, <tt>null</tt> will be returned.
	 * @return the found node, or <tt>null</tt>, if not found.
	 */
	public Node getResult() {
		return this.result;
	}

	/**
	 * Returns <tt>true</tt>, if the node has been found, <tt>false</tt> otherwise.
	 * @return <tt>true</tt>, if the node has been found, <tt>false</tt> otherwise
	 */
	public boolean hasFound() {
		return found;
	}

}
