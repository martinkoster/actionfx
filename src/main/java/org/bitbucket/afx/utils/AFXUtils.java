package org.bitbucket.afx.utils;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bitbucket.afx.annotation.FxView;
import org.bitbucket.afx.view.NodeWrapper;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.ClassPathResource;

import javafx.scene.Node;
import javafx.scene.control.Labeled;
import javafx.scene.control.ProgressIndicator;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;

/**
 * General utility class for working with JavaFX.
 * 
 * @author MartinKoster
 *
 */
public class AFXUtils {

	private static final Log LOG = LogFactory.getLog(AFXUtils.class);

	/**
	 * Finds a node in the scene graph represented by <tt>node</tt> that has the
	 * given <tt>id</tt>.
	 * 
	 * @param node
	 *            the root node of the scene graph to search
	 * @param id
	 *            the node ID to search for
	 * @return the found <tt>javafx.scene.Node</tt>, or <tt>null</tt>, if there
	 *         is no node with the given ID
	 */
	public static Node findNodeById(Node node, String id) {
		FindByNodeIdHandler handler = new FindByNodeIdHandler(id);
		NodeWrapper wrapper = new NodeWrapper(node);
		wrapper.traverse(handler);
		if (handler.hasFound()) {
			return handler.getResult();
		}
		return null;
	}

	/**
	 * Sets the text property of a node identified by <tt>id</tt>.
	 * 
	 * @param id
	 *            the node ID that is an instance of type
	 *            <tt>javafx.scene.control.Labeled</tt>
	 * @param text
	 *            the text to set to the found node
	 * @param root
	 *            the root node of the scene graph to search
	 */
	public static void setLabelText(String id, String text, Node root) {
		Node node = findNodeById(root, id);
		if (node != null && node instanceof Labeled) {
			Labeled labeled = (Labeled) node;
			labeled.setText(text);
		}
	}

	/**
	 * Sets a progress value of a node identified by <tt>id</tt>.
	 * 
	 * @param id
	 *            the node ID that is an instance of
	 *            <tt>javafx.scene.control.ProgressIndicator</tt>
	 * @param value
	 *            the value to set to the progress indicator
	 * @param root
	 *            the root node of the scene graph to search
	 */
	public static void setProgressValue(String id, double value, Node root) {
		Node node = findNodeById(root, id);
		if (node != null && node instanceof ProgressIndicator) {
			ProgressIndicator progressIndicator = (ProgressIndicator) node;
			progressIndicator.setProgress(value);
		}
	}

	/**
	 * Hides a node identified by <tt>id</tt>.
	 * 
	 * @param id
	 *            the node ID of the node that shall be hidden
	 * @param root
	 *            the root node of the scene graph to search
	 */
	public static void hideNode(String id, Node root) {
		Node node = findNodeById(root, id);
		if (node != null) {
			node.setVisible(false);
		}
	}

	/**
	 * Displays a node identified by <tt>id</tt>.
	 * 
	 * @param id
	 *            the node ID of the node that shall be displayed
	 * @param root
	 *            the root node of the scene graph to search
	 */
	public static void displayNode(String id, Node root) {
		Node node = findNodeById(root, id);
		if (node != null) {
			node.setVisible(true);
		}
	}

	/**
	 * Loads an FXML file and returns its root node.
	 * 
	 * @param fxmlFile
	 *            the FXML file to load
	 * @return the root of the scene graph, loaded from the given
	 *         <tt>fxmlFile</tt>
	 */
	public static Node loadFxml(String fxmlFile) {

		try {
			return FXMLLoader.load(AFXUtils.class.getResource(fxmlFile));
		} catch (IOException e) {
			throw new RuntimeException("Can not load file '" + fxmlFile + "'",
					e);
		}
	}

	/**
	 * Loads an FXML file using the specified <tt>controller</tt> instance and
	 * returns the root node of the scene graph.
	 * 
	 * @param fxmlFile
	 *            the FXML file to load
	 * @param controller
	 *            the controller that will be associated with the given
	 *            <tt>fxmlFile</tt>. <b>Please note:</b> This requires that the FXML
	 *            file does not declare its own controller in the file.
	 * @return the root of the scene graph, loaded from the given
	 *         <tt>fxmlFile</tt>
	 */
	public static Node loadFxml(String fxmlFile, Object controller) {
		InputStream fxmlStream = null;
		try {
			ClassPathResource resource = new ClassPathResource(fxmlFile,
					controller.getClass());
			fxmlStream = resource.getInputStream();
			FXMLLoader loader = new FXMLLoader();
			loader.setController(controller);
			return loader.load(fxmlStream);
		} catch (IOException e) {
			throw new RuntimeException("Can not load file '" + fxmlFile + "'",
					e);
		} finally {
			if (fxmlStream != null) {
				try {
					fxmlStream.close();
				} catch (IOException e) {
					throw new RuntimeException("Can not load file '" + fxmlFile
							+ "'", e);
				}
			}
		}
	}

	/**
	 * Determines the view ID for the given controller class <tt>controller</tt>
	 * 
	 * 
	 * @param controller the controller class that shall be checked
	 * @return the view ID that the given controller class is handling
	 */
	public static String determineViewIdForController(Class<?> controller) {
		if (controller == null) {
			return null;
		}
		// extract FxView annotation
		FxView jfxView = AnnotationUtils.findAnnotation(controller,
				FxView.class);
		if (jfxView == null) {
			return null;
		} else {
			return jfxView.id();
		}
	}

	/**
	 * Checks, if the given <tt>value</tt> is <tt>null</tt> or empty.
	 *
	 * @param value
	 *            the value to check
	 * @return <tt>true</tt>, if the value is <tt>null</tt> or empty, <tt>false</tt> otherwise.
	 */
	public static boolean isEmpty(Object value) {
		if (value == null) {
			return true;
		}
		if (value instanceof String) {
			return StringUtils.trimToNull((String) value) == null;
		}
		if (value instanceof StringProperty) {
			return StringUtils.trimToNull(((StringProperty) value).get()) == null;
		}
		if (value instanceof ObservableValue<?>) {
			return ((ObservableValue<?>) value).getValue() == null;
		}
		return false;
	}

	/**
	 * Wrapper for <tt>Runnable</tt> that does not swallow exceptions.
	 * 
	 * @param r the <tt>Runnable</tt> to be wrapped
	 * @return the wrapper instance
	 */
	public static Runnable getWrapper(final Runnable r) {
		return new Runnable() {

			@Override
			public void run() {
				try {
					r.run();
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
					throw e;
				}
			}
		};
	}
}
