package org.bitbucket.afx.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.annotation.AnnotationUtils;

/**
 * Util-class for working with the reflection API.
 * 
 * @author Martin
 *
 */
public class ReflectionUtils extends org.springframework.util.ReflectionUtils{

	/**
	 * Finds all methods that are annotated by the given <tt>annotationClazz</tt>.
	 * 
	 * @param clazz the class to be checked for annotated methods
	 * @param annotationClazz the annotation class to be looked up for
	 * @return the method carrying the given annotation, or <tt>null</tt>, if no method is carrying this annotation.
	 */
	public static Method findAnnotatedMethod(Class<?> clazz, Class<? extends Annotation> annotationClazz) {
		Method[] allMethods = getAllDeclaredMethods(clazz);
		if(allMethods == null || allMethods.length == 0) {
			return null;
		}
		for(Method method : allMethods) {
			 Annotation annotation = AnnotationUtils.findAnnotation(method, annotationClazz);
			 if(annotation != null) {
				 return method;
			 }
		}
		return null;
	}
	
	/**
	 * Extracts all fields annotated by <tt>annotationClazz</tt> in class <tt>clazz</tt> and returns a map.
	 * 
	 * @param clazz the class to be checked for annotated fields
	 * @param annotationClazz the annotation class to be looked up for
	 * @return a map, where the <tt>Field</tt> instance is the key, and the value is the instance of the the annotation
	 */
	public static <T extends Annotation> Map<Field, T> findAnnotatedFields(Class<?> clazz, Class<T> annotationClazz) {
		Map<Field, T> fieldMap = new HashMap<Field, T>();
		Field[] fields = clazz.getDeclaredFields();
		if(fields != null) {
			for(Field field : fields) {
				T a = AnnotationUtils.getAnnotation(field, annotationClazz);
				if(a != null) {
					fieldMap.put(field, a);
				}
			}
		}
		return fieldMap;
	}
	
	/**
	 * Takes a list of <tt>instances</tt> and matches them to <tt>parameterTypes</tt> by bringing them in the 
	 * same order than the <tt>parameterTypes</tt>.
	 * 
	 * @param parameterTypes the ordered parameter types 
	 * @param instances the instances that are candidates for the parameter types
	 * @return the instances from the <tt>instances</tt> list, ordered so that these match the <tt>parameterTypes</tt>
	 */
	public static Object[] matchInstancesToParameterTypes(Class<?>[] parameterTypes, Object... instances) {
		Object[] args = new Object[parameterTypes != null ? parameterTypes.length : 0];
		if(parameterTypes != null) {
			for(int i = 0; i < parameterTypes.length; i++) {
				args[i] = determineInstanceByType(parameterTypes[i], instances);
			}
		}
		return args;
	}
	
	/**
	 * Determines an instance of a given <tt>type</tt> from a list of <tt>instances</tt>.
	 *  
	 * @param type the type to search for
	 * @param instances the list of candidates to be searched for the given <tt>type</tt>
	 * @return the instance matching the given <tt>type</tt>, or <tt>null</tt>, if no instance matches the given <tt>type</tt>
	 */
	private static Object determineInstanceByType(Class<?> type, Object... instances) {
		if(instances != null) {
			for(Object instance : instances) {
				if(type.isAssignableFrom(instance.getClass())) {
					return instance;
				}
			}
		}
		return null;
	}

	/**
	 * Checks, whether the given <tt>methodName</tt> of class <tt>clazz</tt> is in the 
	 * stack of execution.
	 * 
	 * @param className the fully qualified class name hosting the method to check for
	 * @param methodName the name of the method that shall be checked whether it is in the stack of execution
	 * @return <tt>true</tt>, if the method is in the stack of execution, <tt>false</tt> otherwise.
	 * 
	 */
	public static boolean isCalledFromMethod(String className, String methodName) {
        StackTraceElement[] cause = Thread.currentThread().getStackTrace();
        
        for (StackTraceElement se : cause) {
        	if(se.getClassName().equals(className) && se.getMethodName().equals(methodName)) {
        		return true;
        	}
        }
        
        return false;
	}
	
}
