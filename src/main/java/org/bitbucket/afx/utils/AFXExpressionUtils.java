package org.bitbucket.afx.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Utility class for evaluating JFX expressions.
 * 
 * @author Martin
 *
 */
public class AFXExpressionUtils {

	/**
	 * Extracts the component name of the given <tt>nestedPath</tt>, which is the first
	 * name in the path. 
	 * 
	 * @param nestedPath the nested path to extract the component name from
	 * @return the resolved component name
	 */
	public static String getComponentName(String nestedPath) {
		if(StringUtils.trimToNull(nestedPath) == null) {
			return null;
		}
		if(nestedPath.indexOf('.') > 0) {
			return nestedPath.substring(0, nestedPath.indexOf('.'));
		}
		return null;		
	}
	
	/**
	 * Extracts the method name of the given <tt>nestedPath</tt>, which is either
	 * the <tt>nestedPath</tt> itself, if there is no '.', or the part after the '.'.
	 * 
	 * @param nestedPath the nested path to extract the method name from
	 * @return the resolved method name
	 */
	public static String getMethodName(String nestedPath) {
		if(StringUtils.trimToNull(nestedPath) == null) {
			return null;
		}
		if(nestedPath.indexOf('.') > 0 && !nestedPath.endsWith(".")) {
			return nestedPath.substring(nestedPath.indexOf('.') + 1);
		}
		return nestedPath;		
		
	}
	
	/**
	 * Extracts a node ID of the given <tt>nestedPath</tt>, which is either
	 * the <tt>nestedPath</tt> itself, if there is no '.', or the part after the '.'.
	 * 
	 * @param nestedPath the nested path to extract the node ID from
	 * @return the resolved node ID
	 */
	public static String getNodeId(String nestedPath) {
		if(StringUtils.trimToNull(nestedPath) == null) {
			return null;
		}
		if(nestedPath.indexOf('.') > 0 && !nestedPath.endsWith(".")) {
			return nestedPath.substring(nestedPath.indexOf('.') + 1);
		}
		return nestedPath;		
		
	}
}
	
