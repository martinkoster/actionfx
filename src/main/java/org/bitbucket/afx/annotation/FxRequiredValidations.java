package org.bitbucket.afx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Helper-annotation to make <code>JFXNotEmptyValidation</code> repeatable. 
 * 
 * @author MartinKoster
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FxRequiredValidations {
	
	public FxRequiredValidation[] value();
}
