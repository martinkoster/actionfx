package org.bitbucket.afx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Method-level annotation for methods in JavaFX controllers. Using this annotations, a
 * flow can be realized by using the attributes <tt>onSuccessView</tt> or <tt>onFailureView</tt>.
 * 
 * @author MartinKoster
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FxAction {

	public String value() default "";
	
	/**
	 * View identified by its id to be displayed on success.
	 * 
	 * @return a view id
	 */
	public String onSuccessView() default "";
	
	/**
	 * View identified by its id to be displayed on failure.
	 * 
	 * @return a view id
	 */
	public String onFailureView() default "";
	
	/**
	 * Method to call before the actual annotated method is invoked. Can be used e.g. for initializing the model.
	 * 
	 * @return a method to be called
	 */
	public String onBefore() default "";

	/**
	 * Method to call after the actual annotated method is invoked. Can be used e.g. for post-processing.
	 * 
	 * @return a method to be called
	 */
	public String onAfter() default "";
	
	/**
	 * If set to <tt>true</tt>, the annotated method is asynchronously invoked by a <tt>javafx.concurrent.Task</tt>
	 * in order not to block the JavaFX thread, which can cause the main thread to freeze. Default is <tt>false</tt>.
	 *  
	 * @return <tt>true</tt>, if the annotated method shall be called in another Thread, <tt>false</tt> otherwise.
	 */
	public boolean asynchronous() default false;

	/**
	 * JavaFX view and node-ID of a progress indicator to use for binding while asynchronous calls, e.g. 
	 * <tt>&lt;view-ID&gt;.&lt;node-ID&gt;</tt>.
	 * 
	 * @return id of a progress indicator in form of <tt>&lt;view-ID&gt;.&lt;node-ID&gt;</tt>
	 */
	public String progressIndicatorId() default "";

	/**
	 * If set to <tt>true</tt>, the annotated method is only called when there are no validation errors.
	 *  
	 * @return <tt>true</tt>, if the annotated method shall be only called, when there are no validation errors, <tt>false</tt> otherwise.
	 * 
	 */
	public boolean requiresSuccessfulValidation() default false;
}
