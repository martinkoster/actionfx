package org.bitbucket.afx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * In case it is not possible to bind one JavaBean property to one component,
 * this annotation can be used to establish a custom binding between a
 * JavaBean and an arbitrary property of one component. This comes handy
 * in case the component exposes more than one property that is interesting
 * for binding. 
 * 
 * @author MartinKoster
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FxBinding {
	
	/**
	 * Path (or nested path) of the annotated property that shall be bound to a JavaFX node.
	 * 
	 * @return the path to the property to be bound. Can be empty, if the annotation is applied directly to the field to be bound.
	 */
	public String path() default "";
	
	/**
	 * The node that shall be used for binding.
	 * 
	 * @return the node ID in the view that shall be bound to the annotated field
	 */
	public String nodeId();
	
	/**
	 * The property method provided by the <tt>javafx.scene.Node</tt> that shall be used for binding.
	 * 
	 * @return the property method name in the <tt>javafx.scene.Node</tt> that holds the <tt>javafx.beans.property.Property</tt>.
	 */
	public String propertyMethod();
	
	/**
	 * Element type to be bound in case this annotation is applied on <tt>java.util.List</tt>,
	 * as type information of Java-Generics will be erased during compilation.
	 * 
	 * @return the type of element
	 */
	public Class<?> elementType() default Object.class;
}
