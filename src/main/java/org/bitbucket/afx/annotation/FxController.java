package org.bitbucket.afx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Class-level annotation to be applied to JavaFX controllers that shall be managed by Spring.
 * 
 * @author MartinKoster
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FxController {
	
	/**
	 * The controller ID that this class is identified by.
	 * 
	 * @return the controller ID
	 */
	public String value() default "";
	
	/**
	 * The controller ID that this class is identified by.
	 * 
	 * @return the controller ID
	 */
	public String id() default "";
}
