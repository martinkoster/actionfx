package org.bitbucket.afx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Method-level annotation for methods in JavaFX controllers, that are defining the view's behavior.
 * <p>
 * Annotated methods are allowed to have the following parameter types (and any combination of these):
 * <ul>
 * 	<li>(void)</li>
 *  <li>View</li>
 *  <li>ViewStateManager</li>
 *  <li>ValidationHandler</li>
 *  <li>AFXComponentCache</li>
 * </ul>
 * 
 * @author MartinKoster
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FxInitBehavior {

}
