package org.bitbucket.afx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation used in combination with <code>FxController</code> annotation to define the view
 * for a JavaFX controller.
 * 
 * @author MartinKoster
 * 
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FxView {

	/**
	 * The ID of the view. Must be unique among all views of this application.
	 * 
	 * @return the view ID
	 */
	public String id();
	
	/**
	 * Path to the FXML file to load for this view. Path is relative to the application's classpath.
	 * 
	 * @return the path to the FXML file
	 */
	public String fxml();

	/**
	 * Specifies whether this view is a modal dialog or a regular window.  Default is <tt>false</tt>.
	 * 
	 * @return <tt>true</tt>, if this view is a modal dialogue, <tt>false</tt> otherwise.
	 */
	public boolean modal() default false;
	
	/**
	 * Specifies whether this view shall be displayed maxized or not. Default is <tt>false</tt>.
	 * 
	 * @return <tt>true</tt>, whether the view shall be maximized, <tt>false</tt> otherwise.
	 */
	public boolean maximized() default false;

	/**
	 * The width of the view (however <tt>maximized</tt> has a higher priority).
	 * 
	 * @return the width of the view
	 */
	public int sizeX() default -1;
	
	/**
	 * The height of the window (however <tt>maximized</tt> has a higher priority).
	 * 
	 * @return the height of the view 
	 */
	public int sizeY() default -1;
	
	/**
	 * The title to be displayed for the given view/window.
	 * 
	 * @return the title of the window
	 */
	public String title() default "";
	
}
