package org.bitbucket.afx.annotation;

import java.lang.annotation.Repeatable;

/**
 * Field-level annotation to mark the field or the path relative to the
 * annotated field as "non-empty" field.
 * 
 * @author MartinKoster
 *
 */
@Repeatable(FxRequiredValidations.class)
public @interface FxRequiredValidation {

	/**
	 * The path to the attribute that shall be validated. Can be empty if the
	 * annotation is directly applied to the field to be validated.
	 * 
	 * @return the path to the attribute that shall be validated
	 */
	public String path() default "";

	/**
	 * The error code under that the error message can be found, in case a
	 * validation error occurs.
	 * 
	 * @return the error code
	 */
	public String code();
}
