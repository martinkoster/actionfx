package org.bitbucket.afx.i18.exception;

import org.springframework.core.NestedRuntimeException;

/**
 * Base exception class for exceptions that carry an code for internationalization.
 * 
 * @author MartinKoster
 *
 */
public class ErrorCodeSupportingException extends NestedRuntimeException {

	private String errorCode;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3252549421398504960L;

	/**
	 * Constructor accepting an error code under that the error message is available.
	 * 
	 * @param errorCode the error code
	 */
	public ErrorCodeSupportingException(String errorCode) {
		super(errorCode);
		this.errorCode = errorCode;
	}

	/**
	 * Constructor accepting an error code under that the error message is available and
	 * the error cause.
	 * 
	 * @param errorCode the error code
	 * @param cause the cause
	 */
	public ErrorCodeSupportingException(String errorCode, Throwable cause) {
		super(errorCode, cause);
		this.errorCode = errorCode;
	}

	/**
	 * Returns the error code.
	 * @return the error code
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * Sets the error code of this exception
	 * @param errorCode the error code to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

}
