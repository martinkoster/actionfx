package org.bitbucket.afx.tools.template;

import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.FieldCallback;

/**
 * Generates table views for Java class files (experimental)
 * 
 * @author Martin
 *
 */
public class DetailViewGenerator {

	/**
	 * Internal class for extracting field names.
	 * 
	 * @author MartinKoster
	 *
	 */
	private static class FieldNameExtractor implements FieldCallback {

		private List<String> fieldList = new ArrayList<String>();

		private BeanWrapper wrapper;

		public FieldNameExtractor(Class<?> clazz) {
			wrapper = new BeanWrapperImpl(clazz);
		}

		@Override
		public void doWith(Field field) throws IllegalArgumentException, IllegalAccessException {
			Class<?> type = field.getType();
			if (wrapper.isReadableProperty(field.getName())) {
				if (type.isAssignableFrom(String.class) || 
 					type.isAssignableFrom(Integer.class)	||
 					type.isAssignableFrom(Float.class)	||
 					type.isAssignableFrom(Double.class)	||
 					type.isAssignableFrom(Boolean.class)	||
 					type.isAssignableFrom(Long.class)	) {
					
					fieldList.add(field.getName());
				}
			}
		}

		public List<String> getFieldList() {
			return fieldList;
		}

	}

	public static Class<?> loadClass(String classFile) throws Exception {
		ClassLoader loader = DetailViewGenerator.class.getClassLoader();
		Class<?> clazz = loader.loadClass(classFile);
		return clazz;
	}

	public static List<String> extractFields(Class<?> clazz) {
		FieldNameExtractor extractor = new FieldNameExtractor(clazz);
		ReflectionUtils.doWithFields(clazz, extractor);
		return extractor.getFieldList();
	}
	
	private static String generateHeaderFxml() {
		StringBuilder b = new StringBuilder();
		b.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n");
		b.append("<?import javafx.geometry.*?>\n");
		b.append("<?import javafx.scene.image.*?>\n");
		b.append("<?import javafx.scene.control.*?>\n");
		b.append("<?import java.lang.*?>\n");
		b.append("<?import javafx.scene.layout.*?>\n");
		b.append("<?import org.controlsfx.control.*?>\n\n");
		
		b.append("<VBox maxHeight=\"-Infinity\" maxWidth=\"-Infinity\" minHeight=\"-Infinity\" minWidth=\"-Infinity\" prefHeight=\"400.0\" prefWidth=\"600.0\" xmlns:fx=\"http://javafx.com/fxml/1\" xmlns=\"http://javafx.com/javafx/8\">\n");
		b.append("  <children>\n");
		
		return b.toString();
	}
	
	private static String generateFooterFxml() {
		StringBuilder b = new StringBuilder();
		b.append("	</children>");
		b.append("</VBox>");
		return b.toString();
	}	
	
	public static void generateFxml(String output, List<String> fields, String readOnly) throws Exception {
		FileWriter writer = new FileWriter(output);
		
		StringBuilder b = new StringBuilder();
		b.append(generateHeaderFxml());
		
		for(String field : fields) {
			b.append("		<HBox>\n");
			b.append("          <Label text=\"").append(splitCamelCase(field)).append("  :\"/>").append("<Label id=\"").append(field).append("\"/>\n");
			b.append("		</Hbox>\n");
		}
		
		b.append(generateFooterFxml());
		writer.write(b.toString());
		
		writer.close();
	}

	public static String splitCamelCase(String s) {
		   return s.replaceAll(
		      String.format("%s|%s|%s",
		         "(?<=[A-Z])(?=[A-Z][a-z])",
		         "(?<=[^A-Z])(?=[A-Z])",
		         "(?<=[A-Za-z])(?=[^A-Za-z])"
		      ),
		      " "
		   );
		}	
	
	public static final void main(String[] args) throws Exception {
		if (args == null || args.length != 3) {
			System.out.println("Usage: java DetailViewGenerator <class-file> <fxml-output-file> <readOnly>");
			System.exit(0);
		}
		String input = args[0];
		String output = args[1];
		String readOnly = args[2];
		
		Class<?> clazz = loadClass(input);
		List<String> fields = extractFields(clazz);
		generateFxml(output, fields, readOnly);
	}

}
