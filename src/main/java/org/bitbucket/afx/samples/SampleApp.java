package org.bitbucket.afx.samples;

import javafx.stage.Stage;

import org.bitbucket.afx.AFXApplication;
import org.bitbucket.afx.splash.AFXSplashScreen;
import org.bitbucket.afx.splash.impl.AFXSplashScreenImpl;

/**
 * Main application class. 
 * 
 * @author Martin
 *
 */
public class SampleApp extends AFXApplication {

	private final static String XML_APP_CONTEXT_FILE = "/samples/applicationContext.xml";
	
	@Override
	public void startInternal(Stage stage) throws Exception {
		
		LoginController loginController = (LoginController) this.getApplicationContext().getBean(LoginController.class);
		loginController.showView();
	}

	@Override
	public AFXSplashScreen getSplashScreen() {
		return new AFXSplashScreenImpl("", "/samples/javafx.png");
	}
	
	@Override
	public String getApplicationContextLocation() {
		return XML_APP_CONTEXT_FILE;
	}
	
	public final static void main(String[] args) {
		launch(args);
	}		
}
