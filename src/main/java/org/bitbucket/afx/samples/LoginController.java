package org.bitbucket.afx.samples;

import javafx.application.Platform;
import javafx.event.ActionEvent;

import org.bitbucket.afx.annotation.FxAction;
import org.bitbucket.afx.annotation.FxController;
import org.bitbucket.afx.annotation.FxView;
import org.bitbucket.afx.annotation.FxRequiredValidation;
import org.bitbucket.afx.annotation.FxRequiredValidations;
import org.bitbucket.afx.view.validation.ValidationException;

@FxController("loginController")
@FxView(id="loginView", fxml="/samples/Login.fxml", title="Login")
public class LoginController {

	@FxRequiredValidations({
	@FxRequiredValidation(path="username", code="message_error_username"),
	@FxRequiredValidation(path="password", code="message_error_password")})
	private User user = new User();
	
	@FxAction(onSuccessView="loginView")
	public void showView() {
	}
	
	@FxAction(onSuccessView="mainView", onFailureView="loginView", asynchronous=true, progressIndicatorId="loginView.statusBar", requiresSuccessfulValidation=true)
	public void login(ActionEvent e) throws Exception {
		if(!"test".equals(user.getUsername()) || !"test".equals(user.getPassword())) {
			throw new ValidationException("Wrong username/password!");
		}
	}
	
	public void close(ActionEvent e) {
		Platform.exit();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
