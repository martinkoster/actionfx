package org.bitbucket.afx.samples;

import javafx.application.Platform;
import javafx.event.ActionEvent;

import org.bitbucket.afx.annotation.FxController;
import org.bitbucket.afx.annotation.FxView;

@FxController("mainController")
@FxView(id="mainView", fxml="/samples/Main.fxml", title="Sample ActionFX Application", maximized=true)
public class MainController {

	public void handleClose(ActionEvent e) {
		Platform.exit();
	}
	
}

