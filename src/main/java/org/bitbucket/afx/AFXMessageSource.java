package org.bitbucket.afx;


import java.util.Locale;

import org.springframework.context.NoSuchMessageException;

/**
 * Strategy interface for resolving messages, with support for the parameterization
 * and internationalization of such messages.
 * 
 * @author MartinKoster (but basis taken from Spring)
 *
 */
public interface AFXMessageSource {
	
	/**
	 * Try to resolve the message. Return default message if no message was found.
	 * @param code the code to lookup up, such as 'calculator.noRateSet'. Users of
	 * this class are encouraged to base message names on the relevant fully
	 * qualified class name, thus avoiding conflict and ensuring maximum clarity.
	 * @param args array of arguments that will be filled in for params within
	 * the message (params look like "{0}", "{1,date}", "{2,time}" within a message),
	 * or {@code null} if none.
	 * @param defaultMessage String to return if the lookup fails
	 * @return the resolved message if the lookup was successful;
	 * otherwise the default message passed as a parameter
	 * @see java.text.MessageFormat
	 */
	String getMessage(String code, Object[] args, String defaultMessage);

	/**
	 * Try to resolve the message. Treat as an error if the message can't be found.
	 * @param code the code to lookup up, such as 'calculator.noRateSet'
	 * @param args Array of arguments that will be filled in for params within
	 * the message (params look like "{0}", "{1,date}", "{2,time}" within a message),
	 * or {@code null} if none.
	 * @return the resolved message
	 * @throws NoSuchMessageException if the message wasn't found
	 * @see java.text.MessageFormat
	 */
	String getMessage(String code, Object[] args) throws NoSuchMessageException;
	
	/**
	 * Set a locale to the underlying implementation.
	 * 
	 * @param locale the locale to set
	 */
	public void setLocale(Locale locale);
	
	/**
	 * Gets the locale from the underlying implementation.
	 * 
	 * @return the locale 
	 */
	public Locale getLocale();	
}
