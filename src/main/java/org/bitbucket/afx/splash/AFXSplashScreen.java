package org.bitbucket.afx.splash;

import javafx.concurrent.Task;

/**
 * Interface for splash screens.
 * 
 * @author MartinKoster
 *
 */
public interface AFXSplashScreen {

	/**
	 * Display a splash screen for the given <tt>Task</tt>.
	 * <p>
	 * Implementing classes must construct a <tt>javafx.stage.Stage</tt>, preferable
	 * with a <tt>javafx.scene.control.ProgressBar</tt>, bind all properties of the task
	 * the implementor is interested in and then, show the stage.
	 *  
	 * @param task the task that triggers the display of this splash screen
	 */
	public void showSplashScreen(Task<?> task);
}
