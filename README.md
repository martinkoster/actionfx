# ActionFX 0.0.1#

[TOC]

## Motivation ##

ActionFX is a set of ideas aiming to reduce the amount of boilerplate code a developer has to provide for GUI development in JavaFX. As JavaFX offers a great way to specify views via FXML, there is still a lot of code to provide for databinding, view behavior, validation (not addressed by JavaFX at all), asynchronous actions, etc.

ActionFX was motivated to easily migrate an existing JSF- and Spring-based web application to a JavaFX application. In this web application, the full domain objects were already present, i.e. an introduction of Property-based domain objects would result in a big amount of additional code.

At this early stage, ActionFX is not considered as framework yet due to its yet immature state, but as already said as a "set of ideas".

ActionFX is currently based on the following frameworks:

* **JAVA 8 SE** (for JavaFX itself and lambda expressions)
* **Spring 4.1.4.RELEASE** (Dependency Injection container, views and controllers are managed as Spring beans)
* **AspectJ 1.8.5 Runtime** (for compile-time weaving of aspects into controllers)
* **ControlsFX 8.20.8** (for validation)


For more information, please refer to the [Wiki](https://bitbucket.org/martinkoster/actionfx/wiki).